# vehicle sale repair platform
### This project is a POC of creation a Python/Flask/MySQL/docker-compose based webApp. 
### Prerequisites/dependencies
- Docker and Docker Compose intalled locally

### To launch the project, run command below in a terminal
$docker-compose up
- It may take a while to build the DB and app at first time, then in web browser start the web using below URL
http://localhost:5050/index
### To add demo data run the queries in the src/db/insert_demo_data.sql at mysql backend
- DB name: cs6400
- admin DB user: 'root', password: 'root'
- To log in the 'root' user run command in docker container 'src_db_1' CLI (terminal): _mysql -u root -p_
- Once log in as 'root' user, change DB to cs6400 by command: use cs6400;
- Then add data by running SQL queries in insert_demo_data.sql file: copy the queries in the .sql file and paste to mysql terminal, it will run automatically

### The view the documentation/spec of the project, see the pdf file doc/Jaunty Jalopies.pdf



