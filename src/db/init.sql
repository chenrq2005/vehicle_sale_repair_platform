-- CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
CREATE USER IF NOT EXISTS admin@localhost IDENTIFIED BY 'admin';

-- create a database named cs6400
CREATE DATABASE IF NOT EXISTS cs6400;

-- use/activate/switch to the DB cs6400
use cs6400;

GRANT SELECT, INSERT, UPDATE, DELETE, FILE ON *.* TO 'admin'@'localhost';
GRANT ALL PRIVILEGES ON `admin`.* TO 'admin'@'localhost';
GRANT ALL PRIVILEGES ON `cs6400`.* TO 'admin'@'localhost';
FLUSH PRIVILEGES;

-- --------- Tables --------

-- create a table named Login_User for Privileged Access Users
CREATE TABLE Login_User
(
  userID int(16) NOT NULL AUTO_INCREMENT,
  user_name VARCHAR(20) NOT NULL UNIQUE,
  login_password VARCHAR(20) NOT NULL,
  first_name VARCHAR(60) NOT NULL,
  last_name VARCHAR(40) NOT NULL,
  PRIMARY KEY (userID)
);

CREATE TABLE Inventory_Clerk
(
  user_name VARCHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY (user_name)
);

CREATE TABLE Sales_People
(
  user_name VARCHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY (user_name)
);

CREATE TABLE Service_Writer
(
  user_name VARCHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY (user_name)
);

CREATE TABLE Manager
(
  user_name VARCHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY (user_name)
);

CREATE TABLE Owner
(
  user_name VARCHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY (user_name)
);

-- add original Privileged Access Users
-- INSERT INTO Login_User (first_name, last_name, login_password, user_name)
-- VALUES
--   ('Ray', 'Chen', '1234', 'rchen413'),
--   ('Chris', 'Yuan', '1234', 'zyuan32'),
--   ('Zheng', 'Han', '1234', 'zhan303'),
--   ('Lanxin', 'Zhang', '1234', 'lzhang695'),
--   ('Roland', 'Around', '1234', 'raround01');

-- INSERT INTO  Owner VALUES ('raround01');
-- INSERT INTO Inventory_Clerk  VALUES ('rchen413');
-- INSERT INTO  Service_Writer VALUES ('zhan303');
-- INSERT INTO Manager  VALUES ('zyuan32');
-- INSERT INTO  Sales_People VALUES ('lzhang695');



-- create a table named Customers
CREATE TABLE Customer
(
  customer_ID int(16) NOT NULL AUTO_INCREMENT,
  phone VARCHAR(50) NOT NULL,
  email VARCHAR(50) NULL,
  street VARCHAR(50) NOT NULL,
  city VARCHAR(50) NOT NULL,
  state VARCHAR(50) NOT NULL,
  postal_code VARCHAR(10) NOT NULL,
  PRIMARY KEY (customer_ID)
);

-- create a table named Customer_Person
CREATE TABLE Customer_Person
(
  license_number VARCHAR(50) NOT NULL,
  customer_ID int(16) NOT NULL,
  first_name VARCHAR(60) NOT NULL,
  last_name VARCHAR(40) NOT NULL,
  PRIMARY KEY (license_number)
);

-- create a table named Customer_Business
CREATE TABLE Customer_Business
(
  tax_ID VARCHAR(50) NOT NULL,
  customer_ID int(16) NOT NULL,
  business_name VARCHAR(100) NOT NULL,
  primary_contact_first_name VARCHAR(60) NOT NULL,
  primary_contact_last_name VARCHAR(40) NOT NULL,
  title VARCHAR(150) NOT NULL,
  PRIMARY KEY (tax_ID)
);

-- -- create a table named Primary_Contact_Title
-- CREATE TABLE Primary_Contact_Title
-- (
--   tax_ID VARCHAR(50) NOT NULL,
--   title VARCHAR(60) NOT NULL,
--   PRIMARY KEY (title, tax_ID)
-- );

-- create a table named Vehicle
CREATE TABLE Vehicle
(
  vehicleID int(16) NOT NULL AUTO_INCREMENT,
  vin VARCHAR(60) NOT NULL UNIQUE,
  manufacturer_name VARCHAR(50) NOT NULL,
  model_name VARCHAR(100) NOT NULL,
  model_year int(4) NOT NULL,
  v_description VARCHAR(500) NULL,
  invoice_price float NOT NULL,
  added_user_ID int(16) NOT NULL,
  added_dttm timestamp NOT NULL DEFAULT now(),
  PRIMARY KEY (vehicleID)
);

-- create a table named Vehicle_Color
CREATE TABLE Vehicle_Color
(
  vin VARCHAR(60) NOT NULL,
  color VARCHAR(60) NOT NULL,
  PRIMARY KEY (vin, color)
);

-- create a table named Car
CREATE TABLE Car
(
  carID int(16) NOT NULL AUTO_INCREMENT,
  no_of_doors int NOT NULL,
  vin VARCHAR(60) NOT NULL UNIQUE,
  PRIMARY KEY (carID)
);

-- create a table named Convertible
CREATE TABLE Convertible
(
  convertibleID int(16) NOT NULL AUTO_INCREMENT,
  roof_type VARCHAR(60) NOT NULL,
  back_seat_count int NOT NULL,
  vin VARCHAR(60) NOT NULL UNIQUE, 
  PRIMARY KEY (convertibleID)
);

-- create a table named Truck
CREATE TABLE Truck
(
  truckID int(16) NOT NULL AUTO_INCREMENT,
  cargo_capacity int NOT NULL,
  cargo_cover_type VARCHAR(100) NULL,
  no_rear_axles int NOT NULL,
  vin VARCHAR(60) NOT NULL UNIQUE, 
  PRIMARY KEY (truckID)
);

-- create a table named Van_Minivan
CREATE TABLE Van_Minivan
(
  vanID int(16) NOT NULL AUTO_INCREMENT,
  has_driver_side_backdoor BOOLEAN NOT NULL,
  vin VARCHAR(60) NOT NULL UNIQUE, 
  PRIMARY KEY (vanID)
);

-- create a table named SUV
CREATE TABLE SUV
(
  suvID int(16) NOT NULL AUTO_INCREMENT,
  drivetrain_type VARCHAR(10) NOT NULL,
  no_cupholder int NOT NULL,
  vin VARCHAR(60) NOT NULL UNIQUE,
  PRIMARY KEY (suvID)
);

-- create a table named Manufacturer for choosing when adding vehicle
CREATE TABLE Manufacturer
(
  manufacturerID int(16) NOT NULL AUTO_INCREMENT,
  manufacturer_name VARCHAR(50) NOT NULL UNIQUE,
  PRIMARY KEY (manufacturerID) 
);

-- add manufacturers provided in Appendix
INSERT INTO Manufacturer (manufacturer_name)
VALUES 
  ('Acura'), ('Alfa Romeo'), ('Aston Martin'), ('Audi'), ('Bentley'), ('BMW'),  ('Buick'), ('Cadillac'), ('Chevrolet'), ('Chrysler'), ('Dodge'),
  ('Ferrari'), ('FIAT'), ('Ford'),  ('Freightliner'), ('Genesis'), ('GMC'), ('Honda'), ('Hyundai'), ('INFINITI'), ('Jaguar'), ('Jeep'), ('Kia'),
  ('Lamborghini'), ('Land Rover'), ('Lexus'), ('Lincoln'), ('Lotus'), ('Maserati'), ('MAZDA'), ('McLaren'), ('Mercedes-Benz'), ('MINI'), ('Mitsubishi'), 
  ('Nissan'), ('Porsche'), ('Ram'), ('Rolls-Royce'), ('SAAB'), ('Smart'), ('Subaru'), ('Tesla'), ('Toyota'), ('Vauxhall'), ('Volkswagen'), ('Volvo');


-- create a table named Sales
CREATE TABLE Sale
(
--   saleID int(16) NOT NULL AUTO_INCREMENT,
  vin VARCHAR(60) NOT NULL UNIQUE,
  customer_ID int(16) NOT NULL, 
  purchase_date date NOT NULL,
  sold_price float NOT NULL,
  userID int(16) NOT NULL, 
  PRIMARY KEY (vin)
);

-- create a table named repair
CREATE TABLE Repair
(
  repair_ID int(16) NOT NULL AUTO_INCREMENT,
  vin VARCHAR(60) NOT NULL,
  customer_ID int(16) NOT NULL, 
  start_date date NOT NULL,
  labor_charge float NOT NULL,
  r_description VARCHAR(500) NULL,
  completed_date date NULL,
  odometer int NOT NULL,
  userID int(16) NOT NULL, 
  added_dttm timestamp NOT NULL DEFAULT now(),
  PRIMARY KEY(repair_id),
  UNIQUE KEY (vin, customer_ID, start_date)
);

-- create a table named Used_part
CREATE TABLE Used_Part
(
  part_no VARCHAR(20) NOT NULL,
  repair_ID int NOT NULL, 
  part_vendor VARCHAR(30) NOT NULL,
  part_quantity int NOT NULL,
  part_cost float NOT NULL,
  PRIMARY KEY (part_no, repair_ID)
);

-- Constraints   Foreign Keys: FK_ChildTable_childColumn_ParentTable_parentColumn
ALTER TABLE Owner
  ADD CONSTRAINT fk_Owner_username_Login_User_username FOREIGN KEY (user_name) REFERENCES Login_User (user_name);

ALTER TABLE Inventory_Clerk
  ADD CONSTRAINT fk_InventoryClerk_username_Login_User_username FOREIGN KEY (user_name) REFERENCES Login_User (user_name);

ALTER TABLE Sales_People
  ADD CONSTRAINT fk_SalesPeople_username_Login_User_username FOREIGN KEY (user_name) REFERENCES Login_User (user_name);

ALTER TABLE Service_Writer
  ADD CONSTRAINT fk_ServiceWriter_username_Login_User_username FOREIGN KEY (user_name) REFERENCES Login_User (user_name);

ALTER TABLE Manager
  ADD CONSTRAINT fk_Manager_username_Login_User_username FOREIGN KEY (user_name) REFERENCES Login_User (user_name);

ALTER TABLE Customer_Person
  ADD CONSTRAINT fk_CustomerPerson_customerID_Customers_customerID FOREIGN KEY (customer_ID) REFERENCES Customers (customer_ID);

ALTER TABLE Customer_Business
  ADD CONSTRAINT fk_CustomerBusiness_customerID_Customers_customerID FOREIGN KEY (customer_ID) REFERENCES Customers (customer_ID);

-- ALTER TABLE Primary_Contact_Title
--   -- ADD CONSTRAINT fk_PrimaryContactTitle_name_CustomerBusiness_primarycontactname FOREIGN KEY (name) REFERENCES Customer_Business (primary_contact_name),
--   ADD CONSTRAINT fk_PrimaryContactTitle_taxID_CustomerBusiness_taxID FOREIGN KEY (tax_ID) REFERENCES Customer_Business (tax_ID);
  
ALTER TABLE Vehicle 
  ADD CONSTRAINT fk_Vehicle_addedUserID_LoginUser_userID FOREIGN KEY (added_user_ID) REFERENCES Login_User (userID),
  ADD CONSTRAINT fk_Vehicle_manufacturername_Manufacturer_manufacturername FOREIGN KEY (manufacturer_name) REFERENCES Manufacturer (manufacturer_name);

ALTER TABLE Vehicle_Color
   ADD CONSTRAINT fk_VehicleColor_vin_Vehicle_vin FOREIGN KEY (vin) REFERENCES Vehicle (vin);

ALTER TABLE Car
  ADD CONSTRAINT fk_Car_vin_Vehicle_vin FOREIGN KEY (vin) REFERENCES Vehicle (vin);

ALTER TABLE Convertible
  ADD CONSTRAINT fk_Convertible_vin_Vehicle_vin FOREIGN KEY (vin) REFERENCES Vehicle (vin);

ALTER TABLE Truck
  ADD CONSTRAINT fk_Truck_vin_Vehicle_vin FOREIGN KEY (vin) REFERENCES Vehicle (vin);

ALTER TABLE Van_Minivan
  ADD CONSTRAINT fk_Vanminivan_vin_Vehicle_vin FOREIGN KEY (vin) REFERENCES Vehicle (vin);

ALTER TABLE SUV
  ADD CONSTRAINT fk_SUV_vin_Vehicle_vin FOREIGN KEY (vin) REFERENCES Vehicle (vin);


ALTER TABLE Sale 
  ADD CONSTRAINT fk_Sale_vin_Vehicle_vin FOREIGN KEY (vin) REFERENCES Vehicle (vin),
  ADD CONSTRAINT fk_Sale_customerID_Customer_customerID FOREIGN KEY (customer_ID) REFERENCES Customer (customer_ID),
  ADD CONSTRAINT fk_Sale_userID_LoginUser_username FOREIGN KEY (userID) REFERENCES Login_User (userID);

ALTER TABLE Repair 
  ADD CONSTRAINT fk_Repair_vin_Vehicle_vin FOREIGN KEY (vin) REFERENCES Vehicle (vin),
  ADD CONSTRAINT fk_Repair_userID_LoginUser_userID FOREIGN KEY (userID) REFERENCES Login_User (userID),
  ADD CONSTRAINT fk_Repair_customerID_Customer_customerID FOREIGN KEY (customer_ID) REFERENCES Customer (customer_ID);

ALTER TABLE Used_Part ADD CONSTRAINT fk_UsedPart_repairID_Repair_repairID FOREIGN KEY (repair_ID) REFERENCES Repair (repair_ID);