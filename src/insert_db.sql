ALTER TABLE Repair MODIFY labor_charge float(16) NOT NULL;

INSERT INTO Vehicle (vin, manufacturer_name, model_name,model_year, v_description, invoice_price,
added_user_ID) VALUES ('1GCDT19XXT8117038', 'Acura','MDX','2021','abc','45130',1);

INSERT INTO Vehicle_Color (vin, color) VALUES ('1GCDT19XXT8117038', 'Black');
INSERT INTO Car (vin, no_of_doors) VALUES ('1GCDT19XXT8117038', 4);

INSERT INTO Vehicle (vin, manufacturer_name, model_name,model_year, v_description, invoice_price,
added_user_ID) VALUES ('1GCDT19XXT8134905', 'Toyota','Rav4','2000','abc','1000',1);

INSERT INTO Vehicle_Color (vin, color) VALUES ('1GCDT19XXT8134905', 'Black');
INSERT INTO Car (vin, no_of_doors) VALUES ('1GCDT19XXT8134905', 4);

INSERT INTO Vehicle (vin, manufacturer_name, model_name,model_year, v_description, invoice_price,
added_user_ID) VALUES ('TEDFS1234TSSE4', 'Acura','RDX','2022','abc','450000',1);

INSERT INTO Vehicle_Color (vin, color) VALUES ('TEDFS1234TSSE4', 'Beige');
INSERT INTO Car (vin, no_of_doors) VALUES ('TEDFS1234TSSE4', 4);

DECLARE @CustomerID INT;
INSERT INTO Customer (phone, email, street, city, state, postal_code) VALUES ( '2165645645', 'team020@gamil.com','123 pine st', 'New York',
'New York', '12564') ;
SET @CustomerID = (SELECT LAST_INSERT_ID());
INSERT INTO Customer_Person (first_name, last_name, license_number, customer_ID)
VALUES('Alex', 'Han', '1HD890FN', @CustomerID) ;


INSERT INTO Customer (phone, email, street, city, state, postal_code) VALUES ( '2175414335', 'team050@gamil.com','15 pine st', 'New York',
'New York', '12564') ;
SET @CustomerID = (SELECT LAST_INSERT_ID());
INSERT INTO Customer_Business (tax_ID,  customer_ID, business_name, primary_contact_first_name, primary_contact_last_name, title)
VALUES('45648DFHG', @CustomerID, 'wonderfulcar', 'Frank','Wu','manager') ;

INSERT INTO Repair (vin, customer_ID, start_date, labor_charge, r_description, odometer, userID) VALUES ('TEDFS1234TSSE4', 2, now(), 800.0, ' ',43000, 3);

INSERT INTO Sale (vin, customer_ID, purchase_date, sold_price, userID) VALUES ('1GCDT19XXT8134905', 2, now(), 130000, 3);
