/* Project specific Javascript goes here. */

$(window).scroll(function() {
    if ($(this).scrollTop() > 10) {
        $('#nav').addClass('navbar-scrolled');
    } else {
        $('#nav').removeClass('navbar-scrolled');
    }
});


$(function(){
      $('#Car').show();
      $('.selectOption').change(function(){
        var selected = $(this).find(':selected').text();
        //alert(selected);
        $(".desc").hide();
        $('#' + selected).show();
      });
});
