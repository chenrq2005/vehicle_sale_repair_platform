from typing import List, Dict
from flask import Flask, render_template, request, url_for, redirect
import mysql.connector
import json
import datetime

app = Flask(__name__, static_folder="../app/static")

# DB connection configuration
config = {
    'user': 'root',
    'password': 'root',
    'host': 'db',
    'port': '3306',
    'database': 'cs6400',
}


def df_connect(config):
    conn = mysql.connector.connect(**config)
    conn.time_zone = "-04:00"
    # return mysql.connector.connect(**config)
    return conn

def db_cursor(connection):
    return connection.cursor()


def db_close(connection, cursor):
    cursor.close()
    connection.close()

def colored(r, g, b, text):
    return "\033[38;2;{};{};{}m{} \033[38;2;255;255;255m".format(r, g, b, text)


login_user_name = ""
user_role = ""
user_full_name = ""


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    connection = df_connect(config)
    cur = db_cursor(connection)
    cur.execute(
        "SELECT COUNT(*) from Vehicle where vin not in (select vin from Sale);")
    n_vehicle = cur.fetchall()
    print("n of vehicles: ", n_vehicle)
    db_close(connection, cur)
    print("current logged in username: ", login_user_name)
    return render_template('base.html',  number_of_available_vehicle=str(n_vehicle[0][0]), logged_in=login_user_name, user_role=user_role)


@app.route('/login', methods=['GET', 'POST'])
def login():
    print('checking login')
    if request.method == "POST" and request.form['btn_login'] == 'Login':
        inputs = request.form
        uname = inputs['uname']
        pw = inputs['pw']
        if uname == '':
            return render_template('login.html', error_messages=('To log in username cannot be empty!',),)
        elif pw == '':
            return render_template('login.html', error_messages=('To log in password cannot be empty!',),)

        connection = df_connect(config)
        cur = db_cursor(connection)
        cur.execute(
            "SELECT COUNT(*) from Login_User where user_name = (%s);", [uname])
        uname_count = cur.fetchall()
        uname_count = uname_count[0][0]

        cur.execute(
            "SELECT COUNT(*) from Vehicle where vin not in (select vin from Sale);")
        n_vehicle = cur.fetchall()
        print("n of vehicles available for purchase: ", n_vehicle)

        password_count = 0
        if uname_count == 1:
            cur.execute(
                "SELECT COUNT(*) from Login_User where user_name = (%s) and login_password = (%s);", [uname, pw])
            password_count = cur.fetchall()
            password_count = password_count[0][0]
            if password_count == 0:
                return render_template('login.html', error_messages=(f"The entered password is incorrect for the username '{uname}'!", ),)
            elif password_count == 1:
                query_s = """SELECT a.userID, a.user_name, a.login_password, a.first_name, a.last_name, a.user_role FROM
                                (
                                    SELECT Login_User.*, "owner" AS user_role
                                    FROM Login_User JOIN Owner ON Login_User.user_name=Owner.user_name
                                    UNION
                                    SELECT Login_User.*, "inventory_clerk" AS user_role
                                    FROM Login_User JOIN Inventory_Clerk ON Login_User.user_name=Inventory_Clerk.user_name
                                    UNION
                                    SELECT Login_User.*, "sales_people" AS user_role
                                    FROM Login_User JOIN Sales_People ON Login_User.user_name=Sales_People.user_name
                                    UNION
                                    SELECT Login_User.*, "service_writer" AS user_role
                                    FROM Login_User JOIN Service_Writer ON Login_User.user_name=Service_Writer.user_name
                                    UNION
                                    SELECT Login_User.*, "manager" AS user_role
                                    FROM Login_User JOIN Manager ON Login_User.user_name=Manager.user_name
                                ) as a
                            WHERE user_name = (%s);
                        """
                cur.execute(query_s, [uname])
                user_details = cur.fetchall()
                print(user_details)
                global login_user_name
                login_user_name = user_details[0][1]
                global user_role
                user_role = user_details[0][5]
                global user_full_name
                user_full_name = user_details[0][3] + " " + user_details[0][4]
                print(login_user_name)
                print("Current logged in user is: " + user_full_name)
                print("Current logged in user role is: ", user_role)

                # according to different user role, assign different pages and functions to them!
                if user_role == 'inventory_clerk':
                    return render_template('inventory_clerk.html', full_name=(user_full_name,), number_of_available_vehicle=str(n_vehicle[0][0]), logged_in=login_user_name, user_role=user_role)
                elif user_role == 'manager':
                    return render_template('manager.html', full_name=(user_full_name,), number_of_available_vehicle=str(n_vehicle[0][0]), logged_in=login_user_name, user_role=user_role)
                elif user_role == 'service_writer':
                    return render_template('service_writer.html', full_name=(user_full_name,), logged_in=login_user_name, user_role=user_role)
                elif user_role == 'sales_people':
                    return render_template('sales_people.html', number_of_available_vehicle=str(n_vehicle[0][0]), full_name=(user_full_name,), logged_in=login_user_name, user_role=user_role)
                elif user_role == 'owner':
                    return render_template('owner.html', full_name=(user_full_name,), logged_in=login_user_name, user_role=user_role)
        else:
            return render_template('login.html', error_messages=(f"We cannot find the username '{uname}' in our system!", ),)

        db_close(connection, cur)

    elif request.method == "POST" and request.form['btn_login'] == 'Cancel':
        return redirect(url_for('index'))
    else:
        return render_template('login.html')


@app.route('/logout', methods=['GET', 'POST'])
def logout():
    global login_user_name
    login_user_name = ""
    global user_role
    user_role = ""
    print('After logout, the current login_user_name variable is: ' + login_user_name)
    connection = df_connect(config)
    cur = db_cursor(connection)
    cur.execute(
        "SELECT COUNT(*) from Vehicle where vin not in (select vin from Sale);")
    n_vehicle = cur.fetchall()
    db_close(connection, cur)
    return render_template('base.html',  number_of_available_vehicle=str(n_vehicle[0][0]), logged_in=login_user_name)


@app.route('/search_vehicle', methods=['GET', 'POST'])
def search_vehicle():
    connection = df_connect(config)
    cur = db_cursor(connection)
    cur.execute(
        "SELECT COUNT(*) from Vehicle where vin not in (select vin from Sale);")

    n_vehicle = cur.fetchall()

    no_matching_error = "Sorry, it looks like we don’t have that in stock!"

    if request.method == "POST" and request.form['btn_search'] == 'Search':
        inputs = request.form

        v_vin = ""
        v_sale_status = ""
        search_conditions = "You just searched all unsold vehicles with condition(s): "

        final_out = []
        final_out_kw = []
        # check the user is a previleged access
        if login_user_name != "":
            if inputs['v_vin'] != '':
                print("searching using VIN condition (must be a login user)!")
                v_vin = inputs['v_vin']
                search_conditions = search_conditions + " VIN = " + v_vin
                query = """SELECT V.vin, VT.v_type, V.manufacturer_name, V.model_name, V.model_year, GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price \
                FROM Vehicle as V \
                INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible \
                            UNION select vin, 'Truck' as v_type from Truck UNION select vin, 'Van-Minivan' as v_type from Van_Minivan \
                            UNION select vin, 'SUV' as v_type from SUV) as VT on V.vin=VT.vin \
                INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.vin = %s GROUP BY VC.vin, VT.v_type order by V.vin;"""

                cur.execute(query, [v_vin])
                data = cur.fetchall()
                print(data)
                # db_close(connection, cur)
                final_out.append(data)

        print("final_out length before go to non login user searching features:")
        print(len(final_out))

        v_type = ""
        v_manufacturer  = ""
        v_colors = ""
        v_model_year = ""
        v_invoice_price = ""
        v_key_word = ""

        if v_vin == "":

            sale_status_where = "not in (select vin from Sale)"

            if 'v_sale_status' in inputs:
                v_sale_status = inputs['v_sale_status']
                print("The sale status picked: ", v_sale_status)
                if v_sale_status != "-- select one --":
                    if v_sale_status == "sold_vehicles":
                        sale_status_where = "in (select vin from Sale)"
                        search_conditions = "You just searched all sold vehicles with condition(s): "
                        query = """SELECT V.vin, VT.v_type, V.manufacturer_name, V.model_name, V.model_year, \
                        GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price, "NA" as kw_matched \
                        FROM Vehicle as V \
                        INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible \
                                UNION select vin, 'Truck' as v_type from Truck UNION select vin, 'Van_Minivan' as v_type from Van_Minivan \
                                UNION select vin, 'SUV' as v_type from SUV) as VT on V.vin=VT.vin \
                        INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.vin {where} GROUP BY VC.vin, VT.v_type order by V.vin;"""

                        cur.execute(query.format(where=sale_status_where))
                        data = cur.fetchall()
                        print(
                            "The data length searched with all sold vehicles: ", len(data))
                        final_out.append(data)
                    elif v_sale_status == "all_vehicles":
                        sale_status_where = "in (select vin from Vehicle)"
                        search_conditions = "You just searched all vehicles (sold and unsold) with condition(s): "
                        query = """SELECT V.vin, VT.v_type, V.manufacturer_name, V.model_name, V.model_year, \
                        GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price, "NA" as kw_matched \
                        FROM Vehicle as V \
                        INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible \
                                UNION select vin, 'Truck' as v_type from Truck UNION select vin, 'Van_Minivan' as v_type from Van_Minivan \
                                UNION select vin, 'SUV' as v_type from SUV) as VT on V.vin=VT.vin \
                        INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.vin {where} GROUP BY VC.vin, VT.v_type order by V.vin;"""

                        cur.execute(query.format(where=sale_status_where))
                        data = cur.fetchall()
                        print(
                            "The data length searched with all vehicles (sold and unsold): ", len(data))
                        final_out.append(data)
                    else:
                        query = """SELECT V.vin, VT.v_type, V.manufacturer_name, V.model_name, V.model_year, \
                        GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price, "NA" as kw_matched \
                        FROM Vehicle as V \
                        INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible \
                                UNION select vin, 'Truck' as v_type from Truck UNION select vin, 'Van_Minivan' as v_type from Van_Minivan \
                                UNION select vin, 'SUV' as v_type from SUV) as VT on V.vin=VT.vin \
                        INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.vin {where} GROUP BY VC.vin, VT.v_type order by V.vin;"""

                        cur.execute(query.format(where=sale_status_where))
                        data = cur.fetchall()
                        print(
                            "The data length searched with all unsold vehicles: ", len(data))
                        final_out.append(data)

            print("sale status where condition is: ", sale_status_where)

            if inputs['v_type']!="-- select one --":
                v_type = inputs['v_type']
                search_conditions = search_conditions + " Vehicle type='%s'" %(v_type)
                print(v_type)

                query = """SELECT V.vin, %s as v_type, V.manufacturer_name, V.model_name, V.model_year, \
                        GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price, "NA" as kw_matched \
                        FROM Vehicle as V INNER JOIN {table} as VT on V.vin=VT.vin INNER JOIN Vehicle_Color as VC on V.vin=VC.vin \
                        WHERE V.vin {where}
                        GROUP BY VC.vin order by V.vin; \
                        """
                cur.execute(query.format(
                    table=v_type, where=sale_status_where), [v_type])
                data = cur.fetchall()
                print("The data length searched with v type: ", len(data))
                # db_close(connection, cur)
                
                final_out.append(data)

            if inputs['v_manufacturer']!="-- select one --":
                v_manufacturer = inputs['v_manufacturer']
                search_conditions = search_conditions + " Manufacturer='%s'" % (v_manufacturer)
                print(v_type)

                query = """SELECT V.vin, VT.v_type, V.manufacturer_name, V.model_name, V.model_year, \
                        GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price, "NA" as kw_matched \
                        FROM Vehicle as V \
                        INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible \
                                UNION select vin, 'Truck' as v_type from Truck UNION select vin, 'Van_Minivan' as v_type from Van_Minivan \
                                UNION select vin, 'SUV' as v_type from SUV) as VT on V.vin=VT.vin \
                        INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.manufacturer_name = %s and V.vin {where} GROUP BY VC.vin, VT.v_type order by V.vin;"""

                cur.execute(query.format(where=sale_status_where),
                            [v_manufacturer])
                data = cur.fetchall()
                print("The data length searched with v manufacturer: ", len(data))
                # db_close(connection, cur)
                final_out.append(data)

            if inputs['v_model_year']!="-- select one --":
                v_model_year = inputs['v_model_year']
                search_conditions = search_conditions + " Model year='%s'" % (v_model_year)
                print(v_model_year)

                query = """SELECT V.vin, VT.v_type, V.manufacturer_name, V.model_name, V.model_year, \
                        GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price, "NA" as kw_matched  \
                        FROM Vehicle as V \
                        INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible \
                                UNION select vin, 'Truck' as v_type from Truck UNION select vin, 'Van_Minivan' as v_type from Van_Minivan \
                                UNION select vin, 'SUV' as v_type from SUV) as VT on V.vin=VT.vin \
                        INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.model_year = %s and V.vin {where} GROUP BY VC.vin, VT.v_type order by V.vin;"""

                cur.execute(query.format(
                    where=sale_status_where), [v_model_year])
                data = cur.fetchall()
                print(data)

                final_out.append(data)

            if len(request.form.getlist('v_colors')) > 0:
                v_colors = ' '.join(request.form.getlist('v_colors'))
                search_conditions = search_conditions + \
                    " Color='%s'" % (v_colors)
                print(v_colors)

                query = """SELECT V.vin, VT.v_type, V.manufacturer_name, V.model_name, V.model_year, \
                        GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price, "NA" as kw_matched  \
                        FROM Vehicle as V \
                        INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible \
                                UNION select vin, 'Truck' as v_type from Truck UNION select vin, 'Van_Minivan' as v_type from Van_Minivan \
                                UNION select vin, 'SUV' as v_type from SUV) as VT on V.vin=VT.vin \
                        INNER JOIN Vehicle_Color as VC on V.vin=VC.vin where V.vin {where} GROUP BY VC.vin, VT.v_type having colors = %s order by V.vin;"""

                cur.execute(query.format(where=sale_status_where), [v_colors])
                data = cur.fetchall()
                print(data)
                final_out.append(data)

            if inputs['v_list_price']!="":
                v_list_price = inputs['v_list_price']
                
                v_invoice_price_numeric = round(float(v_list_price)/1.25, 2)
                v_invoice_price = str(v_invoice_price_numeric)
                search_conditions = search_conditions + \
                    " List Price>='%s'" % (v_list_price)

                query = """SELECT V.vin, VT.v_type, V.manufacturer_name, V.model_name, V.model_year, 
                            GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price, "NA" as kw_matched \
                        FROM Vehicle as V \
                        INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible \
                                UNION select vin, 'Truck' as v_type from Truck UNION select vin, 'Van_Minivan' as v_type from Van_Minivan \
                                UNION select vin, 'SUV' as v_type from SUV) as VT on V.vin=VT.vin \
                        INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.invoice_price >= %s and V.vin {where} GROUP BY VC.vin, VT.v_type order by V.vin;"""

                cur.execute(query.format(where=sale_status_where),
                            [v_invoice_price])
                data = cur.fetchall()
                print("length of data searched with list price: ", len(data))
                final_out.append(data)

            if inputs['v_key_word']!="":
                v_key_word = inputs['v_key_word']
                search_conditions = search_conditions + " Key word='%s'" % (v_key_word)
                print(v_key_word)

                query = """SELECT V.vin, VT.v_type, V.manufacturer_name, V.model_name, V.model_year, \
                        GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, V.invoice_price*1.25 as list_price, "X" as kw_matched\
                        FROM Vehicle as V \
                        INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible \
                                UNION select vin, 'Truck' as v_type from Truck UNION select vin, 'Van_Minivan' as v_type from Van_Minivan \
                                UNION select vin, 'SUV' as v_type from SUV) as VT on V.vin=VT.vin \
                        INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.vin {where} and \
                        (V.v_description LIKE '%{keyword}%' or V.manufacturer_name LIKE '%{keyword}%' \
                                        or V.model_year LIKE '%{keyword}%' or V.model_name LIKE '%{keyword}%')  \
                        GROUP BY VC.vin, VT.v_type order by V.vin; \
                        """

                cur.execute(query.format(
                    where=sale_status_where, keyword=v_key_word))
                data = cur.fetchall()
                print(data)
                final_out_kw.append(data)

        print("final_out_kw length: ", len(final_out_kw))

        if v_vin == "" and v_type == "" and v_manufacturer == "" and v_colors == "" and v_model_year == "" and v_invoice_price == "" and v_key_word == "" and v_sale_status == "":
            search_conditions = "You did not pick any searching condition, please pick at least one!"
            return render_template('view_vehicle.html', search_condition=(search_conditions, ), logged_in=login_user_name, user_role=user_role)

        check_list = None
        result = None
        result_with_kw = []
        if v_vin != "" or v_type != "" or v_manufacturer != "" or v_colors != "" or v_model_year != "" or v_invoice_price != "" or v_sale_status != "":
            if len(final_out) > 0:
                check_list = list(set(l) for l in final_out[1:])
                result = set(final_out[0])
                for s in check_list:
                    result = result.intersection(s)

                if v_key_word != "":
                    if len(result) > 0 and len(final_out_kw) and len(final_out_kw[0]) > 0:
                        for tuple_kw in final_out_kw[0]:
                            for tuple in result:
                                if tuple_kw[0] == tuple[0]:
                                    result_with_kw.append(tuple_kw)
                                    break
                        result = result_with_kw
                    else:
                        result = None
                else:
                    pass
            elif len(final_out) == 0:
                if v_key_word != "":
                    result = None
        else:
            if v_key_word != "":
                if len(final_out_kw) and len(final_out_kw[0]) > 0:
                    result = final_out_kw[0]

        data_out = []
        if result is not None:
            for d in result:
                d_temp = list(d)
                d_temp[6] = '{:.2f}'.format(d_temp[6])
                data_out.append(d_temp)
                # sort the list with the tuple element vin
                data_out.sort(key=lambda x: x[0])

            print("the length data out is ", len(data_out))
            if len(data_out) > 0:
                no_matching_error = ''
                return render_template('view_vehicle.html', search_condition=(search_conditions, ), data=data_out,
                            error_messages=(no_matching_error, ), logged_in=login_user_name, user_role=user_role)
            else:
                return render_template('view_vehicle.html', search_condition=(search_conditions, ),
                            error_messages=(no_matching_error, ), logged_in=login_user_name, user_role=user_role)
        else:
            return render_template('view_vehicle.html', search_condition=(search_conditions, ),
                            error_messages=(no_matching_error, ), logged_in=login_user_name, user_role=user_role)
        #
    
    elif request.method == "POST" and request.form['btn_search'] == 'Clear All':
        connection = df_connect(config)
        cur = db_cursor(connection)
        cur.execute("SELECT manufacturer_name from Manufacturer;")
        manufacturers = cur.fetchall()
        manufacturers = [i[0] for i in manufacturers]
        return render_template('search_base.html', number_of_available_vehicle=str(n_vehicle[0][0]), logged_in=login_user_name, user_role=user_role, manufacturers=manufacturers)
    else:
        connection = df_connect(config)
        cur = db_cursor(connection)
        cur.execute("SELECT manufacturer_name from Manufacturer;")
        manufacturers = cur.fetchall()
        manufacturers = [i[0] for i in manufacturers]
        return render_template('search_base.html', number_of_available_vehicle=str(n_vehicle[0][0]), logged_in=login_user_name, user_role=user_role, manufacturers=manufacturers) 


@app.route('/view_vehicle_details/<rowData>', methods=['GET', 'POST'])
def view_vehicle_details(rowData):
    connection = df_connect(config)
    cur = db_cursor(connection)
    cur.execute("SELECT COUNT(*) from Vehicle;")
    n_vehicle = cur.fetchall()

    rowData_list = rowData.split(',')
    print(rowData_list)
    v_vin = rowData_list[0].split('[')[1].strip("'")
    print(v_vin)
    v_type = rowData_list[1].strip().strip("'")
    print(v_type)
    # for elem in rowData:
    #     print(elem)

    query = """SELECT V.vin, V.manufacturer_name, V.model_name, V.model_year, GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, \
                V.invoice_price*1.25 as list_price, V.invoice_price, V.v_description, %s as v_type, VT.* \
                FROM Vehicle as V INNER JOIN {table} as VT on V.vin=VT.vin \
                INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.vin=%s \
                GROUP BY VC.vin order by V.vin; \
            """
    cur.execute(query.format(table=v_type), [v_type, v_vin])

    details_data = cur.fetchall()
    print(details_data)

    detail_data_out = []
    for d in details_data:
        d_temp = list(d)
        d_temp[5] = '{:.2f}'.format(d_temp[5])
        d_temp[6] = '{:.2f}'.format(d_temp[6])
        detail_data_out.append(d_temp)

    return render_template('view_vehicle_details.html', detail_data=detail_data_out, logged_in=login_user_name, user_role=user_role)


@app.route('/add_vehicle', methods=['GET', 'POST'])
def add_vehicle():
    if request.method == "POST" and request.form['btn_add_v'] == 'Add Vehicle':
        inputs = request.form
        vin = inputs['vin']

        connection = df_connect(config)
        cur = db_cursor(connection)
        cur.execute("SELECT COUNT(*) from Vehicle where vin=%s;", [vin])
        n_vehicle_with_inputVin = cur.fetchall()
        # n_vehicle_with_inputVin[0][0]==0 means this is a new vehicle upon VIN, other wise it is already in the system!
        if n_vehicle_with_inputVin[0][0] == 0:
            manufacturer = inputs['manufacturer']
            model = inputs['model']
            model_year = int(inputs['model_year'])
            if (model_year > datetime.datetime.now().year + 1):
                raise ValueError(
                    "Model years cannot exceed the current year plus one!")
            colors = request.form.getlist('colors')
            v_description = inputs['description']
            invoice_price = float(inputs['invoice_price'])

            connection = df_connect(config)
            cur = db_cursor(connection)

            now = datetime.datetime.utcnow()
            # now = datetime.datetime.now()
            cur.execute(
                "SELECT COUNT(*), userID FROM Login_User WHERE user_name = (%s);", [login_user_name])
            data = cur.fetchall()
            print(data)
            userID = data[0][1]
            print([vin, manufacturer, model, model_year,
                  v_description, invoice_price, userID])
            try:
                cur.execute("INSERT into Vehicle (vin, manufacturer_name, model_name, model_year, v_description, invoice_price, added_user_ID) \
                    VALUES ((%s), (%s),(%s),(%s),(%s),(%s),(%s));", [vin, manufacturer, model, model_year, v_description, invoice_price, userID])
                """ cur.execute( "INSERT into Vehicle (vin, manufacturer_name, model_name, model_year, v_description, invoice_price, added_user_ID) \
                        VALUES ((%s), (%s), (%s), (%s), (%s), (%s), (%s));\
                            INSERT INTO Vehicle_Color (vin, color) VALUES ((%s), (%s));" \
                                ,[vin, manufacturer, model, model_year, v_description, invoice_price, userID, vin, ' '.join(colors)], multi=True) """
                connection.commit()
                print(cur.rowcount, 'record was inserted Vehicle table')

                print(colors)
                for color in colors:
                    # cur.execute( "INSERT INTO Vehicle_Color (vin, color) VALUES ((%s), (%s));",[vin, " ".join(color)])
                    cur.execute(
                        "INSERT INTO Vehicle_Color (vin, color) VALUES ((%s), (%s));", [vin, color])
                connection.commit()
                print(cur.rowcount, 'record was inserted Vehicle_Color table')
            except Exception as e:
                print(e)

            v_type = inputs['v_type']
            print(v_type)
            try:
                if v_type == "Car":
                    no_of_doors = int(inputs['n_doors'])
                    query = "INSERT into Car (no_of_doors, vin) VALUES ((%s), (%s))"
                    val = (no_of_doors, vin)
                    cur.execute(query, val)
                    print(cur.rowcount, 'record was inserted Car table')
                elif v_type == "Convertible":
                    query = "INSERT into Convertible (roof_type, back_seat_count, vin) VALUES ((%s), (%s), (%s))"
                    val = (inputs['roof_type'], int(
                        inputs['back_seat_count']), vin)
                    cur.execute(query, val)
                    print(cur.rowcount, 'record was inserted Convertible table')
                elif v_type == "Truck":
                    query = "INSERT into Truck (cargo_capacity, cargo_cover_type, no_rear_axles, vin) VALUES ((%s), (%s), (%s), (%s))"
                    val = (int(inputs['cargo_capacity']), inputs['cargo_cover_type'], int(
                        inputs["n_rear_axles"]), vin)
                    cur.execute(query, val)
                    print(cur.rowcount, 'record was inserted Truck table')
                elif v_type == "Van":
                    query = "INSERT into Van_Minivan (has_driver_side_backdoor, vin) VALUES ((%s), (%s))"
                    if inputs['has_driver_side_back_door'] == 'Yes':
                        val = (1, vin)
                    else:
                        val = (0, vin)
                    cur.execute(query, val)
                    print(cur.rowcount, 'record was inserted Van_Minivan table')
                elif v_type == "SUV":
                    query = "INSERT into SUV (drivetrain_type, no_cupholder, vin) VALUES ((%s), (%s), (%s))"
                    val = (inputs['drivetrain_type'], int(inputs['n_cup_holders']), vin)
                    cur.execute(query, val)
                    print(cur.rowcount, 'record was inserted SUV table')
            except Exception as e:
                print(e)

            connection.commit()

            cur.execute(
                "SELECT COUNT(*) from Manufacturer where manufacturer_name=%s;", [manufacturer])
            n_manufacturer = cur.fetchall()    

            # n_manufacturer[0][0] == 0 means the manufacturer_name is not in the table Manufacturer
            if n_manufacturer[0][0] == 0:
                cur.execute("INSERT into Manufacturer (manufacturer_name) VALUES ((%s));", [manufacturer])
                print(cur.rowcount, 'record was inserted Manufacturer table')
                connection.commit()
               

            cur.execute("SELECT manufacturer_name from Manufacturer;")
            manufacturers = cur.fetchall()
            manufacturers = [i[0] for i in manufacturers]

            query = """SELECT V.vin, V.manufacturer_name, V.model_name, V.model_year, GROUP_CONCAT(VC.color SEPARATOR ' ') as colors, \
                V.invoice_price*1.25 as list_price, V.invoice_price, V.v_description, %s as v_type, VT.* \
                FROM Vehicle as V INNER JOIN {table} as VT on V.vin=VT.vin \
                INNER JOIN Vehicle_Color as VC on V.vin=VC.vin WHERE V.vin=%s \
                GROUP BY VC.vin order by V.vin; \
            """
            cur.execute(query.format(table=v_type), [v_type, vin])

            details_data = cur.fetchall()
            print(details_data)

            detail_data_out = []
            for d in details_data:
                d_temp = list(d)
                d_temp[5] = '{:.2f}'.format(d_temp[5])
                d_temp[6] = '{:.2f}'.format(d_temp[6])
                detail_data_out.append(d_temp)

            db_close(connection, cur)

            # return render_template('add_vehicle.html', logged_in=login_user_name, user_role=user_role, manufacturers=manufacturers, v_added_messages=('Great! You just successfully added one vehicle in our system!',))
            return render_template('view_vehicle_details.html', detail_data=detail_data_out, logged_in=login_user_name, user_role=user_role, page_from='add_vehicle')
        else:
            cur.execute("SELECT manufacturer_name from Manufacturer;")
            manufacturers = cur.fetchall()
            manufacturers = [i[0] for i in manufacturers]
            db_close(connection, cur)
            return render_template('add_vehicle.html', logged_in=login_user_name, user_role=user_role, manufacturers=manufacturers, v_exist_messages=('The VIN of the vehicle you just entered was already in our system!!!',))
    elif request.method == "POST" and request.form['btn_add_v'] == 'Cancel':
        return render_template('inventory_clerk.html', full_name=(user_full_name,), logged_in=login_user_name, user_role=user_role)
    else:
        connection = df_connect(config)
        cur = db_cursor(connection)
        cur.execute("SELECT manufacturer_name from Manufacturer;")
        manufacturers = cur.fetchall()
        manufacturers = [i[0] for i in manufacturers]
        # print(manufacturers)
        return render_template('add_vehicle.html', logged_in=login_user_name, user_role=user_role, manufacturers=manufacturers)


@app.route('/add_sale', methods=['GET', 'POST'])
def add_sale():
    if 'v_vin' in request.args:
        v_vin = request.args.get('v_vin')
    else:
        v_vin = ''

    print(v_vin)
    return render_template('add_sale.html', logged_in=login_user_name, user_role=user_role, v_vin = v_vin)

@app.route('/add_sale_ajax', methods=['GET', 'POST'])
def add_sale_ajax():
    if request.method == "POST":
        connection = df_connect(config)
        cur = db_cursor(connection)

        message = "Add sale failed"
        inputs = request.form
        print(inputs)
        v_vin = inputs['v_vin']
        cur.execute(
            "SELECT COUNT(*), invoice_price FROM Vehicle WHERE vin = (%s);", [v_vin])
        data = cur.fetchall()
        invoice_price = data[0][1]

        customer_id = int(inputs['customer_id'])
        purchase_date = inputs['purchase_date']
        sold_price = float(inputs['sold_price'])

        print(purchase_date)
        cur.execute(
            "SELECT COUNT(*), userID FROM Login_User WHERE user_name = (%s);", [login_user_name])
        data = cur.fetchall()
        print(data)
        userID = data[0][1]
        if data[0][0] == 0:
            message = "Please log in first!"
            db_close(connection, cur)
            return message

        if sold_price < 0.95*invoice_price:  # TODO: userrole constraint
            if user_role != "owner":
                message = "sold_price is lower than 0.95*invoice_price"
            else:
                try:
                    cur.execute("INSERT INTO Sale (vin, customer_ID, purchase_date, sold_price, userID)  \
                                VALUES ((%s), (%s), (%s), (%s), (%s));", [v_vin, customer_id, purchase_date, sold_price, userID])
                    message = "Successfully added sale! NOTE:sold_price is lower than 0.95*invoice_price "
                    connection.commit()
                    print(cur.rowcount, 'record was inserted')
                except Exception as e:
                    print(e)
                    message = "Add sale failed. Please make sure the vehicle is not sold and price is reasonable."
                db_close(connection, cur)
        else:
            try:
                cur.execute("INSERT INTO Sale (vin, customer_ID, purchase_date, sold_price, userID)  \
                            VALUES ((%s), (%s), (%s), (%s), (%s));", [v_vin, customer_id, purchase_date, sold_price, userID])
                message = "Successfully added sale!"
                connection.commit()
                print(cur.rowcount, 'record was inserted')
            except Exception as e:
                print(e)
                message = "Add sale failed. Please make sure the vehicle is not sold and price is reasonable."
            db_close(connection, cur)
        return message
        # return render_template('add_repair.html', message = message)
    else:
        return render_template('add_sale.html', logged_in=login_user_name, user_role=user_role)


@app.route('/search_vehicle_ajax', methods=['GET', 'POST'])
def search_vehicle_ajax():
    connection = df_connect(config)
    cur = db_cursor(connection)

    message = ""
    inputs = request.form
    print(inputs)
    v_vin = inputs['v_vin']
    if v_vin is not None:
        cur.execute(
            "SELECT COUNT(*) FROM Vehicle where vin = (%s) order by vin;", [v_vin])
        data_in_stock = cur.fetchall()

        cur.execute(
            "SELECT COUNT(*) FROM Vehicle where vin = (%s) and vin not in (select vin from Sale) order by vin;", [v_vin])
        data_in_stock_and_not_sold = cur.fetchall()

        cur.execute(
            "SELECT COUNT(*), model_year, model_name, manufacturer_name, GROUP_CONCAT(color SEPARATOR ' '), purchase_date FROM Vehicle \
                INNER JOIN Vehicle_Color ON Vehicle.vin = Vehicle_Color.vin \
                INNER JOIN Sale ON Vehicle.vin = Sale.vin \
                where Vehicle.vin = (%s) and Vehicle.vin in (select vin from Sale) order by Vehicle.vin;", [v_vin])
        data_in_stock_and_sold = cur.fetchall()

        cur.execute( "SELECT COUNT(*) FROM Repair where vin = (%s) and completed_date is NULL;", [v_vin])
        data_has_open_repair = cur.fetchall()

        cur.execute( "SELECT * FROM Repair where vin = (%s) and completed_date is NULL;", [v_vin])
        open_repair_data = cur.fetchall()

        db_close(connection, cur)

        if data_in_stock[0][0] == 0:
            message = 'The vehicle is not in our system!'
        elif data_in_stock[0][0] > 0 and data_in_stock_and_not_sold[0][0] > 0:
            message = 'The vehicle is our system, but has not been sold!'
        elif data_in_stock[0][0] > 0 and data_in_stock_and_sold[0][0] > 0 and data_has_open_repair[0][0] > 0:
            message = 'The vehicle is a sold, but now has an open repair, see below!'
        elif data_in_stock[0][0] > 0 and data_in_stock_and_sold[0][0] > 0 and data_has_open_repair[0][0] == 0:
            message = 'The vehicle was sold and now has NO open repair, it is eligible for adding new repair!'
        print(message)
    return render_template('search_vehicle_ajax.html', result1 =data_in_stock, result=data_in_stock_and_sold, open_repair_data=open_repair_data, v_vin=v_vin, message=message)


@app.route('/search_vehicle_add_sale_ajax', methods=['GET', 'POST'])
def search_vehicle_add_sale_ajax():
    connection = df_connect(config)
    cur = db_cursor(connection)

    message = ""
    inputs = request.form
    print(inputs)
    v_vin = inputs['v_vin']
    if v_vin is not None:
        cur.execute(
            "SELECT COUNT(*) FROM Vehicle where vin = (%s) order by vin;", [v_vin])
        data_in_stock = cur.fetchall()

        cur.execute(
            "SELECT COUNT(*),model_year, model_name, manufacturer_name, GROUP_CONCAT(color SEPARATOR ' ') FROM Vehicle \
                INNER JOIN Vehicle_Color ON Vehicle.vin = Vehicle_Color.vin \
                    where Vehicle.vin = (%s) and Vehicle.vin not in (select vin from Sale) order by Vehicle.vin;", [v_vin])
        data_in_stock_and_not_sold = cur.fetchall()

        cur.execute(
            "SELECT COUNT(*), model_year, model_name, manufacturer_name, GROUP_CONCAT(color SEPARATOR ' '), purchase_date FROM Vehicle \
                INNER JOIN Vehicle_Color ON Vehicle.vin = Vehicle_Color.vin \
                INNER JOIN Sale ON Vehicle.vin = Sale.vin \
                where Vehicle.vin = (%s) and Vehicle.vin in (select vin from Sale) order by Vehicle.vin;", [v_vin])
        data_in_stock_and_sold = cur.fetchall()
        db_close(connection, cur)

        if data_in_stock[0][0] == 0:
            message = 'The vehicle is not in our system!'
        elif data_in_stock[0][0] > 0 and data_in_stock_and_sold[0][0] > 0:
            message = 'The vehicle is already sold!'
        elif data_in_stock[0][0] > 0 and data_in_stock_and_not_sold[0][0] > 0:
            message = 'The vehicle is in our system, and is ready to be sold!'
        print(message)
    return render_template('search_vehicle_ajax.html', result1=data_in_stock, result=data_in_stock_and_not_sold, v_vin=v_vin, message=message)


@app.route('/search_customer_ajax', methods=['GET', 'POST'])
def search_customer_ajax():
    connection = df_connect(config)
    cur = db_cursor(connection)

    message = "Sorry, customer doesn't exist!"
    inputs = request.form
    print(inputs)
    c_id = inputs['c_id']
    c_type = inputs['c_type']
    # customer_ID = 'no customer_ID'
    if c_id is not None:
        if c_type == "Person":
            cur.execute("SELECT COUNT(*),  Customer.customer_ID, CONCAT(first_name,' ',last_name) as name,  phone, email, CONCAT(street,', ',state,' ',postal_code) as address\
                    FROM Customer_Person INNER JOIN Customer ON Customer_Person.customer_id = Customer.customer_id \
                    where license_number = (%s);", [c_id])
        elif c_type == "Business":
            cur.execute("SELECT COUNT(*), Customer.customer_ID, business_name, phone, email, CONCAT(street,', ',state,' ',postal_code) as address \
                    FROM Customer_Business INNER JOIN Customer ON Customer_Business.customer_id = Customer.customer_id \
                    where tax_ID = (%s);", [c_id])

        data = cur.fetchall()

        # need to convert invoice price to list price to display
        data_out = []

        if data[0][0] > 0:
            message = 'customer found'
        print(message)
    db_close(connection, cur)
    return render_template('search_customer_ajax.html', result=data, c_id=c_id, message=message)


@app.route('/add_repair', methods=['GET', 'POST'])
def add_repair():
    return render_template('add_repair.html', logged_in=login_user_name, user_role=user_role)

@app.route('/add_repair_ajax', methods=['GET', 'POST'])
def add_repair_ajax():
    if request.method == "POST" and request.form['addtype'] == 'part':
        connection = df_connect(config)

        message = "Add part failed"
        inputs = request.form
        print(inputs)
        # repairid = int(inputs['repairid'])
        part_id = inputs['part_id']
        part_vendor = inputs['part_vendor']
        part_cost = inputs['part_cost']
        part_quantity = inputs['part_quantity']
        v_vin = inputs['v_vin']
        customer_id = inputs['customer_id']
        start_date = datetime.datetime.utcnow().date()
        if customer_id == '':
            return "Cannot find customer"
        else:
            customer_id = int(inputs['customer_id'])
        cur = db_cursor(connection)
        try:
            cur.execute("SELECT COUNT(*), repair_ID FROM Repair WHERE vin=(%s) AND customer_ID = (%s) AND start_date = (%s);", [
                        v_vin, customer_id, start_date])
            data = cur.fetchall()
            print(data)
            if data[0][0] == 0:
                message = "Cannot locate repair ID"
                return message
            else:
                repairid = data[0][1]
            
        except Exception as e:
            print(e)
            
       

        try:
            cur.execute("SELECT COUNT(*) FROM Used_Part WHERE repair_ID = (%s) AND part_no = (%s);",
                        [repairid, part_id])
            data = cur.fetchall()
            if data[0][0] > 0:
                message = "Cannot add part. The part has been added!"
                return message
            
        except Exception as e:
            print(e)

       


        try:
            cur.execute("INSERT INTO Used_Part (repair_ID, part_no, part_vendor, part_cost, part_quantity) VALUES ((%s), (%s), (%s), (%s), (%s));", [
                        repairid, part_id, part_vendor, part_cost, part_quantity])
            message = "Successfully added part!"
            connection.commit()
            db_close(connection, cur)
            return message
        except Exception as e:
            print(e)
        
        return message
    elif request.method == "POST" and request.form['addtype'] == 'repair':
        connection = df_connect(config)
        cur = db_cursor(connection)

        message = "Add repair failed. Please make sure customer and vehicles are valid!"
        inputs = request.form
        print(inputs)
        v_vin = inputs['v_vin']
        customer_id = int(inputs['customer_id'])
        start_date = datetime.datetime.utcnow().date()
        v_odometer = int(inputs['v_odometer'])
        v_labor = float(inputs['v_labor'])
        r_description = inputs['r_description']
        #TODO: complete_date
        print(login_user_name)
        cur.execute(
            "SELECT COUNT(*), userID FROM Login_User WHERE user_name = (%s);", [login_user_name])
        data = cur.fetchall()
        print(data)
        userID = data[0][1]
        if data[0][0] == 0:
            message = "Please log in first!"
            db_close(connection, cur)
            return message

        cur.execute(
            "SELECT COUNT(*) FROM Repair WHERE vin = (%s) AND customer_ID = (%s) AND start_date = (%s);", [v_vin, customer_id, start_date])
        data = cur.fetchall()
        print(data)
        print(start_date)
        if data[0][0] > 0:
            message = "Repair already exists!"
            db_close(connection, cur)
            return message

        try:
            cur.execute("INSERT INTO Repair (vin, customer_ID, start_date, labor_charge, r_description,  \
                        odometer, userID) VALUES ((%s), (%s), (%s), (%s), (%s), (%s),(%s));", [v_vin, customer_id, start_date, v_labor, r_description,
                                                                                               v_odometer, userID])
            message = "Successfully added repair!"
            connection.commit()
        except Exception as e:
            print(e)
        db_close(connection, cur)
        return message
        # return render_template('add_repair.html', message = message)
    elif request.method == "POST" and request.form['addtype'] == 'update':
        connection = df_connect(config)
        cur = db_cursor(connection)

        message = "Update Labor changes failed. Please make sure vehicles are valid!"
        inputs = request.form
        print(inputs)
        v_vin = inputs['v_vin']
        v_labor_charge = float(inputs['v_labor'])
        #TODO: complete_date
        print(login_user_name)

        cur.execute(
            "SELECT COUNT(*), userID FROM Login_User WHERE user_name = (%s);", [login_user_name])
        data = cur.fetchall()
        print(data)
        userID = data[0][1]
        if data[0][0] == 0:
            message = "Please log in first!"
            db_close(connection, cur)
            return message

        cur.execute("SELECT COUNT(*) FROM Repair WHERE vin = (%s) and completed_date is NULL;", [v_vin])
        data_has_open_repair = cur.fetchall()

        cur.execute("SELECT labor_charge FROM Repair WHERE vin = (%s) and completed_date is NULL;", [v_vin])
        current_labor_charge = cur.fetchall()
        if data_has_open_repair[0][0] > 0:
            if user_role == "service_writer" and current_labor_charge[0][0] > v_labor_charge:
               message = "The updates to labor charges cannot be less than the previous value!!!"
               return message 
            else:
                try:
                    cur.execute("UPDATE Repair set labor_charge = %s where vin=%s and completed_date is NULL;", [
                                v_labor_charge, v_vin])
                    message = "Successfully updated labor charges on the open repair from %s to %s!" %(current_labor_charge[0][0], v_labor_charge)
                    connection.commit()
                except Exception as e:
                    print(e)
            db_close(connection, cur)
        return message
    elif request.method == "POST" and request.form['addtype'] == 'close':
        connection = df_connect(config)
        cur = db_cursor(connection)

        message = "Update Labor changes failed. Please make sure vehicles are valid!"
        inputs = request.form
        print(inputs)
        v_vin = inputs['v_vin']

        print(login_user_name)

        cur.execute(
            "SELECT COUNT(*), userID FROM Login_User WHERE user_name = (%s);", [login_user_name])
        data = cur.fetchall()
        print(data)
        userID = data[0][1]
        # if data[0][0] == 0:
        #     message = "Please log in first!"
        #     db_close(connection, cur)
        #     return message

        cur.execute("SELECT COUNT(*) FROM Repair WHERE vin = (%s) and completed_date is NULL;", [v_vin])
        data = cur.fetchall()

        cur.execute("SELECT repair_ID FROM Repair WHERE vin = (%s) and completed_date is NULL;", [v_vin])
        repairID = cur.fetchall()

        completed_date = datetime.datetime.utcnow().date()

        if data[0][0] > 0:
            try:
                cur.execute("UPDATE Repair set completed_date = %s where repair_ID = %s;", [completed_date, repairID[0][0]])
                message = "The repair has just been successfully closed/completed!"
                connection.commit()
            except Exception as e:
                print(e)
            db_close(connection, cur)
        return message
    else:
        return render_template('add_repair.html', logged_in=login_user_name, user_role=user_role)


@app.route('/add_customer', methods=['GET', 'POST'])
def add_customer():
    if request.method == "POST":
        connection = df_connect(config)
        cur = db_cursor(connection)

        message = "Add customer failed"
        inputs = request.form
        print(inputs)
        c_type = inputs["c_type"]
        phone = inputs["phone"]
        email = inputs["email"]
        street = inputs["street"]
        city = inputs["city"]
        state = inputs["state"]
        postal_code = inputs["postal_code"]
        license_number = inputs["license_number"]
        first_name = inputs["first_name"]
        last_name = inputs["last_name"]
        tax_ID = inputs["tax_ID"]
        business_name = inputs["business_name"]
        c_first_name = inputs["c_first_name"]
        c_last_name = inputs["c_last_name"]
        title = inputs["title"]

        if c_type == 'Person':
            try:
                """ cur.execute(" DECLARE @CustomerID INT; INSERT INTO Customer (phone, email, street, city, state, postal_code) \
                        VALUES ( (%s), (%s),(%s), (%s),(%s), (%s)); \
                SET @CustomerID = (SELECT LAST_INSERT_ID()); \
                    INSERT INTO Customer_Person (first_name, last_name, license_number, customer_ID) \
                        VALUES((%s), (%s), (%s), @CustomerID) ;",[phone, email, street, city, state, postal_code, first_name, last_name, license_number],multi=True) """

                cur.execute("INSERT INTO Customer (phone, email, street, city, state, postal_code) VALUES ( (%s), (%s),(%s), (%s),(%s), (%s)); ", [
                            phone, email, street, city, state, postal_code])
                print(cur.rowcount, 'record was inserted')
                connection.commit()
                #cur.execute(" DECLARE @CustomerID INT;")
                cur.execute("SET @CustomerID = (SELECT LAST_INSERT_ID());")
                cur.execute("INSERT INTO Customer_Person (first_name, last_name, license_number, customer_ID) \
                        VALUES((%s), (%s), (%s), @CustomerID) ;", [first_name, last_name, license_number])
                connection.commit()
                print(cur.rowcount, 'record was inserted')
                message = "Customer successfully added!"

            except Exception as e:
                print("customer cannot be added")
                print(e)

        else:
            try:
                cur.execute("INSERT INTO Customer (phone, email, street, city, state, postal_code) VALUES ( (%s), (%s),(%s), (%s),(%s), (%s)); ", [
                            phone, email, street, city, state, postal_code])
                print(cur.rowcount, 'record was inserted')
                connection.commit()
                #cur.execute(" DECLARE @CustomerID INT;")
                cur.execute("SET @CustomerID = (SELECT LAST_INSERT_ID());")
                cur.execute("INSERT INTO Customer_Business (tax_ID, customer_ID, business_name, primary_contact_first_name, primary_contact_last_name, title) \
                        VALUES((%s), @CustomerID, (%s), (%s), (%s), (%s)) ;", [tax_ID, business_name, c_first_name, c_last_name, title])
                connection.commit()
                print(cur.rowcount, 'record was inserted')
                message = "Customer successfully added!"
            except Exception as e:
                print("customer cannot be added")
                print(e)

        db_close(connection, cur)
        return message
        # return render_template('add_repair.html', message = message)
    else:
        return render_template('add_customer.html', logged_in=login_user_name, user_role=user_role)


@app.route('/view_sales/<value>', methods=['GET', 'POST'])
def view_sales(value):
    print('*********************************', value, type(value))

    # row_list = value.split(',')
    # print('$$$$$$$$$$$$$$$$$',row_list)
    # print('aaaaaaaaaaa', row_list[0])
    # v_list = row_list[0].split("'")
    # # print(v_list)
    # v_vin = v_list[1]
    v_vin = value
    print('```````````````````', v_vin)

    no_matching_error = ''
    view_sale_result = ""

    connection = df_connect(config)
    cur = db_cursor(connection)

    query_view_sale_result = ("""
    SELECT Sale.vin, Sale.purchase_date, Sale.sold_price, C_list.c_name, C_list.phone, C_list.email, C_list.c_address, CONCAT(Login_User.first_name, " ", Login_User.last_name) AS s_name
    FROM Sale 
    LEFT JOIN 
    (SELECT CONCAT(Customer_Person.first_name, " ", Customer_Person.last_name) AS c_name, Customer.customer_ID, Customer.phone, Customer.email, CONCAT(Customer.street, ", ", Customer.city, ", ", Customer.state, ", ", Customer.postal_code) AS c_address
    FROM Customer INNER JOIN Customer_Person ON Customer.customer_ID = Customer_Person.customer_ID
    UNION
    SELECT CONCAT(Customer_Business.business_name, " ", Customer_Business.title, ": ", Customer_Business.primary_contact_first_name, " ", Customer_Business.primary_contact_last_name)AS c_name, Customer.customer_ID, Customer.phone, Customer.email, CONCAT(Customer.street, ", ", Customer.city, ", ", Customer.state, ", ", Customer.postal_code) AS c_address
    FROM Customer INNER JOIN Customer_Business ON Customer.customer_ID = Customer_Business.customer_ID) AS C_list ON Sale.customer_ID = C_list.customer_ID
    LEFT JOIN Login_User ON Sale.userID = Login_User.userID
    WHERE Sale.vin = '%s';""" % (v_vin))
    cur.execute(query_view_sale_result)
    view_sale_result = cur.fetchall()
    # print(view_sale_result)

    if len(view_sale_result) == 0:
        no_matching_error = "No sale result. Please add sale."

        # return render_template('add_sale.html', logged_in=login_user_name, user_role=user_role, message=no_matching_error, v_vin = v_vin)
        # link to add sale page

    db_close(connection, cur)

    return render_template('view_sales.html', v_vin = v_vin, data=view_sale_result, error_messages=no_matching_error, logged_in=login_user_name, user_role=user_role)


@app.route('/view_vehicle', methods=['GET', 'POST'])
def view_vehicle():
    return render_template('view_vehicle.html')


@app.route('/view_repairs', methods=['GET', 'POST'])
def view_repairs():
    if request.method == "POST":
        connection = df_connect(config)
        cur = db_cursor(connection)

        messages = "Sorry, no such vehicle!"
        inputs = request.form

        print(inputs)
        v_vin = inputs['v_vin']

        if v_vin is not None:
            cur.execute(
                "SELECT COUNT(*) FROM Vehicle where vin = (%s) order by vin;", [v_vin])
            data = cur.fetchall()

            if data[0][0] > 0:  # meaning there is such vehicle in the system
                # if there is such vehicle in the system then check if sold?
                cur.execute(
                    "SELECT COUNT(*) FROM Sale WHERE vin=(%s);", [v_vin])
                data1 = cur.fetchall()

                if data1[0][0] == 0:
                    messages = "Vehicle not sold yet"
                    return render_template('view_repairs_from_detail.html', error_messages=(messages,), logged_in=login_user_name, user_role=user_role)
                else:  # now we determined that vin is in the system and it's  been sold and we should go ahead listing out the details of the repairs..

                    # show vehicle details frist.....
                    cur.execute(
                        "SELECT Vehicle.vin, VT.v_type, manufacturer_name, model_name, model_year, GROUP_CONCAT(Vehicle_Color.color SEPARATOR ' '), v_description \
                        FROM Vehicle left join Vehicle_Color ON Vehicle.vin = Vehicle_Color.vin \
                        INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible UNION select vin, \
                        'Truck' as v_type from Truck UNION select vin, 'Van-Minivan' as v_type from Van_Minivan UNION select vin, 'SUV' as v_type from SUV)\
                        as VT on Vehicle.vin=VT.vin WHERE Vehicle.vin=(%s) GROUP BY Vehicle_Color.vin, VT.v_type order by vin;", [v_vin])
                    # do not need to list list price since it's been sold   # could select from the vehicle table join sale table...
                    sold_vehicle_info = cur.fetchall()

                    if len(sold_vehicle_info) > 0:
                        messages = ''

                    if user_role == 'manager' or user_role == 'owner':

                        # query_s = """SELECT CONCAT(Customer_Person.first_name, ' ', Customer_Person.last_name) AS individual_name,\
                        #     Customer_Business.business_name, CONCAT(Login_User.first_name, ' ' , Login_User.last_name) AS\
                        #     service_writer, Repair.start_date, Repair.completed_date, Repair.labor_charge,\
                        #     FORMAT(SUM(Used_Part.part_cost),2) AS parts_cost, FORMAT((Repair.labor_charge + SUM(Used_Part.part_cost)),2) AS\
                        #     total_cost FROM Repair LEFT JOIN Customer_Person ON Repair.customer_ID=Customer_Person.customer_ID\
                        #     LEFT JOIN Customer_Business ON Repair.customer_ID=Customer_Business.customer_ID\
                        #     LEFT JOIN Used_Part on Repair.repair_ID=Used_Part.repair_ID \
                        #     LEFT JOIN Login_User on Repair.userID = Login_User.userID WHERE Repair.vin = (%s) \
                        #     GROUP BY Customer_Person.first_name, Customer_Person.last_name, Customer_Business.business_name, \
                        #     Login_User.first_name, Login_User.last_name, Repair.start_date, Repair.completed_date, Repair.labor_charge,\
                        #     Used_Part.repair_ID;"""
                        query_s = ("""
                                SELECT C_list.c_name, Repair.start_date, Repair.completed_date, Repair.vin, Repair.odometer, ROUND(UP.total_part_cost,2), \
                                    ROUND(Repair.labor_charge,2), ROUND((UP.total_part_cost + Repair.labor_charge),2) AS total_cost, CONCAT(Login_User.first_name, " ", Login_User.last_name) AS s_name
                                FROM Repair INNER JOIN (SELECT SUM(Used_Part.part_cost) AS total_part_cost, Used_Part.repair_ID
                                            FROM Used_Part GROUP BY repair_ID) AS UP ON Repair.repair_ID = UP.repair_ID
                                LEFT JOIN
                                (SELECT CONCAT(Customer_Person.first_name, " ", Customer_Person.last_name) AS c_name, Customer.customer_ID
                                FROM Customer INNER JOIN Customer_Person ON Customer.customer_ID = Customer_Person.customer_ID
                                UNION
                                SELECT Customer_Business.business_name AS c_name, Customer.customer_ID
                                FROM Customer INNER JOIN Customer_Business ON Customer.customer_ID = Customer_Business.customer_ID) AS C_list ON Repair.customer_ID = C_list.customer_ID
                                LEFT JOIN Login_User ON Repair.userID = Login_User.userID
                                WHERE Repair.vin = '%s'
                                ORDER BY Repair.start_date DESC, Repair.completed_date DESC, Repair.vin ASC;""" % (v_vin))

                        # cur.execute(query_repair_income)

                        cur.execute(query_s)

                        data2 = cur.fetchall()
                        print("data2: ", data2)
                        db_close(connection, cur)

                        repair_vehicle_info = []

                        if len(data2) > 0:
                            # loop through the returned result data2 which is a list of tuples, if individual name is empty, then the customer is business, vice versa
                            # pass that data to the rendered page.
                            messages = ' Repair Details'

                            return render_template('view_repairs.html', data0=sold_vehicle_info, repair_data=data2, error_messages=(messages,),
                                                   logged_in=login_user_name, user_role=user_role)
                        else:
                            messages = 'no repair history for this vin'
                            return render_template('view_repair.html', data0=sold_vehicle_info, error_messages=(messages,), logged_in=login_user_name, user_role=user_role)
                    else:
                        return render_template('view_repairs.html', data0=sold_vehicle_info, error_messages=(messages,), logged_in=login_user_name, user_role=user_role)
            else:
                return render_template('view_repairs.html', error_messages=(messages,), logged_in=login_user_name, user_role=user_role)

        return render_template('view_repairs.html', logged_in=login_user_name, user_role=user_role)


@app.route('/view_repairs_from_detail/<value>', methods=['GET', 'POST'])
def view_repairs_from_detail(value):

    v_vin = value
    print('```````````````````', v_vin)
    connection = df_connect(config)
    cur = db_cursor(connection)

    messages = "Sorry, no such vehicle!"
    # inputs = request.form

    # print(inputs)
    # v_vin = inputs['v_vin']

    if v_vin is not None:
        cur.execute(
            "SELECT COUNT(*) FROM Vehicle where vin = (%s) order by vin;", [v_vin])
        data = cur.fetchall()

        if data[0][0] > 0:  # meaning there is such vehicle in the system
            # if there is such vehicle in the system then check if sold?
            cur.execute(
                "SELECT COUNT(*) FROM Sale WHERE vin=(%s);", [v_vin])
            data1 = cur.fetchall()

            if data1[0][0] == 0:
                messages = "Vehicle not sold yet"
                return render_template('view_repairs_from_detail.html', error_messages=(messages,), logged_in=login_user_name, user_role=user_role)
            else:  # now we determined that vin is in the system and it's  been sold and we should go ahead listing out the details of the repairs..

                # show vehicle details frist.....
                cur.execute(
                    "SELECT Vehicle.vin, VT.v_type, manufacturer_name, model_name, model_year, GROUP_CONCAT(Vehicle_Color.color SEPARATOR ' '), v_description \
                    FROM Vehicle left join Vehicle_Color ON Vehicle.vin = Vehicle_Color.vin \
                    INNER JOIN (select vin, 'Car' as v_type from Car UNION select vin, 'Convertible' as v_type from Convertible UNION select vin, \
                    'Truck' as v_type from Truck UNION select vin, 'Van-Minivan' as v_type from Van_Minivan UNION select vin, 'SUV' as v_type from SUV)\
                    as VT on Vehicle.vin=VT.vin WHERE Vehicle.vin=(%s) GROUP BY Vehicle_Color.vin, VT.v_type order by vin;", [v_vin])
                # do not need to list list price since it's been sold   # could select from the vehicle table join sale table...
                sold_vehicle_info = cur.fetchall()

                if len(sold_vehicle_info) > 0:
                    messages = ''

                if user_role == 'manager' or user_role == 'owner':

                    # query_s = """SELECT CONCAT(Customer_Person.first_name, ' ', Customer_Person.last_name) AS individual_name,\
                    #     Customer_Business.business_name, CONCAT(Login_User.first_name, ' ' , Login_User.last_name) AS\
                    #     service_writer, Repair.start_date, Repair.completed_date, Repair.labor_charge,\
                    #     FORMAT(SUM(Used_Part.part_cost),2) AS parts_cost, FORMAT((Repair.labor_charge + SUM(Used_Part.part_cost)),2) AS\
                    #     total_cost FROM Repair LEFT JOIN Customer_Person ON Repair.customer_ID=Customer_Person.customer_ID\
                    #     LEFT JOIN Customer_Business ON Repair.customer_ID=Customer_Business.customer_ID\
                    #     LEFT JOIN Used_Part on Repair.repair_ID=Used_Part.repair_ID \
                    #     LEFT JOIN Login_User on Repair.userID = Login_User.userID WHERE Repair.vin = (%s) \
                    #     GROUP BY Customer_Person.first_name, Customer_Person.last_name, Customer_Business.business_name, \
                    #     Login_User.first_name, Login_User.last_name, Repair.start_date, Repair.completed_date, Repair.labor_charge,\
                    #     Used_Part.repair_ID;"""
                    query_s = ("""
                            SELECT C_list.c_name, Repair.start_date, Repair.completed_date, Repair.vin, Repair.odometer, ROUND(UP.total_part_cost,2), \
                                 ROUND(Repair.labor_charge,2), ROUND((UP.total_part_cost + Repair.labor_charge),2) AS total_cost, CONCAT(Login_User.first_name, " ", Login_User.last_name) AS s_name
                            FROM Repair INNER JOIN (SELECT SUM(Used_Part.part_cost) AS total_part_cost, Used_Part.repair_ID
                                        FROM Used_Part GROUP BY repair_ID) AS UP ON Repair.repair_ID = UP.repair_ID
                            LEFT JOIN
                            (SELECT CONCAT(Customer_Person.first_name, " ", Customer_Person.last_name) AS c_name, Customer.customer_ID
                            FROM Customer INNER JOIN Customer_Person ON Customer.customer_ID = Customer_Person.customer_ID
                            UNION
                            SELECT Customer_Business.business_name AS c_name, Customer.customer_ID
                            FROM Customer INNER JOIN Customer_Business ON Customer.customer_ID = Customer_Business.customer_ID) AS C_list ON Repair.customer_ID = C_list.customer_ID
                            LEFT JOIN Login_User ON Repair.userID = Login_User.userID
                            WHERE Repair.vin = '%s'
                            ORDER BY Repair.start_date DESC, Repair.completed_date DESC, Repair.vin ASC;""" % (v_vin))

                    # cur.execute(query_repair_income)

                    cur.execute(query_s)

                    data2 = cur.fetchall()
                    print("data2: ", data2)

                    repairs_vehicle_info = []
                    repair_vehicle_info = []

                    if len(data2) > 0:
                        # loop through the returned result data2 which is a list of tuples, if individual name is empty, then the customer is business, vice versa
                        # pass that data to the rendered page.
                        messages = ' Repair Details'

                        db_close(connection, cur)
                        print("transferred data: ", repairs_vehicle_info)
                        return render_template('view_repairs_from_detail.html', data0=sold_vehicle_info, repair_data=data2, error_messages=(messages,),
                                               logged_in=login_user_name, user_role=user_role)
                    else:
                        messages = 'no repair history for this vin'
                        return render_template('view_repairs_from_detail.html', data0=sold_vehicle_info, error_messages=(messages,), logged_in=login_user_name, user_role=user_role)
                else:
                    return render_template('view_repairs_from_detail.html', data0=sold_vehicle_info, error_messages=(messages,), logged_in=login_user_name, user_role=user_role)
        else:
            return render_template('view_repairs_from_detail.html', error_messages=(messages,), logged_in=login_user_name, user_role=user_role)

    return render_template('view_repairs_from_detail.html', logged_in=login_user_name, user_role=user_role)


@app.route('/view_reports', methods=['GET', 'POST'])
def view_reports():
    return render_template('view_reports.html', logged_in=login_user_name, user_role=user_role)


@app.route('/sales_by_color_report', methods=['GET', 'POST'])
def sales_by_color():
    connection = df_connect(config)
    cur = db_cursor(connection)
    sale_by_color = ""
    latest_sale_date = ""

    query_latest_sale_date = """
    SELECT DATE_FORMAT(MAX(purchase_date), '%Y-%m-%d') FROM Sale;"""

    cur.execute(query_latest_sale_date)

    latest_sale_date = cur.fetchall()
    # print(latest_sale_date, type(latest_sale_date))

    yymmdd = latest_sale_date[0][0]
    # print(yymmdd, type(yymmdd))
    


    # this query will show all colors even if there is no sales record
    query_sale_by_color = ("""
    SELECT Color.color, IFNULL(month_sold_data.sold_in_month,0), IFNULL(year_sold_data.sold_in_year,0), IFNULL(all_sold_data.sold_all_time,0) FROM(

        (SELECT DISTINCT VC.color FROM Vehicle 
            INNER JOIN 
            (SELECT CN_list.vin, "Multiple" AS color FROM (
                SELECT Vehicle_Color.vin, COUNT(Vehicle_Color.vin) AS n_color 
                FROM Vehicle_Color
                GROUP BY Vehicle_Color.vin) AS CN_list
            WHERE CN_list.n_color > 1

            UNION

            SELECT simple_color_vin.vin, Vehicle_Color.color FROM (SELECT CN_list.vin FROM(
                SELECT Vehicle_Color.vin, COUNT(Vehicle_Color.vin) AS n_color 
                FROM Vehicle_Color
                GROUP BY Vehicle_Color.vin) AS CN_list
            WHERE CN_list.n_color = 1) AS simple_color_vin
            INNER JOIN Vehicle_Color ON simple_color_vin.vin = Vehicle_Color.vin) AS VC ON VC.vin = Vehicle.vin) AS Color

        LEFT JOIN

        (SELECT sd_all.color, IFNULL(COUNT(sd_all.vin),0) AS sold_all_time FROM (
            SELECT sold_vehicle.vin, sold_vehicle.color, DATEDIFF('%s', sold_vehicle.purchase_date) AS sell_days 
            FROM (
                SELECT Vehicle.vin, Sale.purchase_date, VC.color FROM Vehicle INNER JOIN Sale ON Vehicle.vin = Sale.vin 
                INNER JOIN 
                    (SELECT CN_list.vin, "Multiple" AS color FROM (
                        SELECT Vehicle_Color.vin, COUNT(Vehicle_Color.vin) AS n_color 
                        FROM Vehicle_Color
                        GROUP BY Vehicle_Color.vin) AS CN_list
                    WHERE CN_list.n_color > 1
                    UNION
                    SELECT simple_color_vin.vin, Vehicle_Color.color FROM (SELECT CN_list.vin FROM(
                        SELECT Vehicle_Color.vin, COUNT(Vehicle_Color.vin) AS n_color 
                        FROM Vehicle_Color
                        GROUP BY Vehicle_Color.vin) AS CN_list
                    WHERE CN_list.n_color = 1) AS simple_color_vin
                    INNER JOIN Vehicle_Color ON simple_color_vin.vin = Vehicle_Color.vin) AS VC ON VC.vin = Sale.vin) AS sold_vehicle) AS sd_all
        WHERE sd_all.sell_days >0
        GROUP BY sd_all.color) AS all_sold_data ON Color.color = all_sold_data.color

        LEFT JOIN

        (SELECT sd365.color, COUNT(sd365.vin) AS sold_in_year FROM (
            SELECT sold_vehicle.vin, sold_vehicle.color, DATEDIFF('%s', sold_vehicle.purchase_date) AS sell_days 
            FROM (
                SELECT Vehicle.vin, Sale.purchase_date, VC.color FROM Vehicle INNER JOIN Sale ON Vehicle.vin = Sale.vin 
                INNER JOIN 
                    (SELECT CN_list.vin, "Multiple" AS color FROM (
                        SELECT Vehicle_Color.vin, COUNT(Vehicle_Color.vin) AS n_color 
                        FROM Vehicle_Color
                        GROUP BY Vehicle_Color.vin) AS CN_list
                    WHERE CN_list.n_color > 1
                    UNION
                    SELECT simple_color_vin.vin, Vehicle_Color.color FROM (SELECT CN_list.vin FROM(
                        SELECT Vehicle_Color.vin, COUNT(Vehicle_Color.vin) AS n_color 
                        FROM Vehicle_Color
                        GROUP BY Vehicle_Color.vin) AS CN_list
                    WHERE CN_list.n_color = 1) AS simple_color_vin
                    INNER JOIN Vehicle_Color ON simple_color_vin.vin = Vehicle_Color.vin) AS VC ON VC.vin = Sale.vin) AS sold_vehicle) AS sd365
        WHERE sd365.sell_days <=365
        GROUP BY sd365.color) AS year_sold_data ON all_sold_data.color = year_sold_data.color

        LEFT JOIN

        (SELECT sd30.color, COALESCE(COUNT(sd30.vin),0) AS sold_in_month FROM (
            SELECT sold_vehicle.vin, sold_vehicle.color, DATEDIFF('%s', sold_vehicle.purchase_date) AS sell_days 
            FROM (
                SELECT Vehicle.vin, Sale.purchase_date, VC.color FROM Vehicle INNER JOIN Sale ON Vehicle.vin = Sale.vin 
                INNER JOIN 
                    (SELECT CN_list.vin, "Multiple" AS color FROM (
                        SELECT Vehicle_Color.vin, COUNT(Vehicle_Color.vin) AS n_color 
                        FROM Vehicle_Color
                        GROUP BY Vehicle_Color.vin) AS CN_list
                    WHERE CN_list.n_color > 1
                    UNION
                    SELECT simple_color_vin.vin, Vehicle_Color.color FROM (SELECT CN_list.vin FROM(
                        SELECT Vehicle_Color.vin, COUNT(Vehicle_Color.vin) AS n_color 
                        FROM Vehicle_Color
                        GROUP BY Vehicle_Color.vin) AS CN_list
                    WHERE CN_list.n_color = 1) AS simple_color_vin
                    INNER JOIN Vehicle_Color ON simple_color_vin.vin = Vehicle_Color.vin) AS VC ON VC.vin = Sale.vin) AS sold_vehicle) AS sd30
        WHERE sd30.sell_days <=30
        GROUP BY sd30.color) AS month_sold_data ON all_sold_data.color = month_sold_data.color);""" %(yymmdd, yymmdd, yymmdd))

    cur.execute(query_sale_by_color)

    sale_by_color = cur.fetchall()

    # print(sale_by_color)
    db_close(connection, cur)
    return render_template('sales_by_color_report.html', data=sale_by_color, logged_in=login_user_name, user_role=user_role)


@app.route('/sales_by_type_report', methods=['GET', 'POST'])
def sales_by_type():
    connection = df_connect(config)
    cur = db_cursor(connection)
    sale_by_type = ""
    latest_sale_date = ""

    query_latest_sale_date = """
    SELECT DATE_FORMAT(MAX(purchase_date), '%Y-%m-%d') FROM Sale;"""

    cur.execute(query_latest_sale_date)

    latest_sale_date = cur.fetchall()
    # print(latest_sale_date, type(latest_sale_date))

    yymmdd = latest_sale_date[0][0]
    # print(yymmdd, type(yymmdd))

    query_sale_by_type = ("""
    SELECT AVT.type, IFNULL(month_sold_data.sold_in_month,0), IFNULL(year_sold_data.sold_in_year,0), IFNULL(all_sold_data.sold_all_time,0) FROM(

        (SELECT DISTINCT VT.type
        FROM Vehicle 
        LEFT JOIN (SELECT Vehicle.*, "Car" AS type FROM Vehicle INNER JOIN Car ON Vehicle.vin = Car.vin
                    UNION
                    SELECT Vehicle.*, "Convertible" AS type FROM Vehicle INNER JOIN Convertible ON Vehicle.vin = Convertible.vin 
                    UNION
                    SELECT Vehicle.*, "Truck" AS type FROM Vehicle INNER JOIN Truck ON Vehicle.vin = Truck.vin
                    UNION
                    SELECT Vehicle.*, "Van" AS type FROM Vehicle INNER JOIN Van_Minivan ON Vehicle.vin = Van_Minivan.vin 
                    UNION
                    SELECT Vehicle.*, "SUV" AS type FROM Vehicle INNER JOIN SUV ON Vehicle.vin = SUV.vin) AS VT ON VT.vin = Vehicle.vin) AS AVT

        LEFT JOIN

        (SELECT sd_all.type, COUNT(sd_all.vin) AS sold_all_time FROM (
            SELECT sold_vehicle.vin, sold_vehicle.type, DATEDIFF('%s', sold_vehicle.purchase_date) AS sell_days 
            FROM (
                SELECT Temp0.type, Sale.vin, Sale.purchase_date
        FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin 
        LEFT JOIN (SELECT Vehicle.*, "Car" AS type FROM Vehicle INNER JOIN Car ON Vehicle.vin = Car.vin
                    UNION
                    SELECT Vehicle.*, "Convertible" AS type FROM Vehicle INNER JOIN Convertible ON Vehicle.vin = Convertible.vin 
                    UNION
                    SELECT Vehicle.*, "Truck" AS type FROM Vehicle INNER JOIN Truck ON Vehicle.vin = Truck.vin
                    UNION
                    SELECT Vehicle.*, "Van" AS type FROM Vehicle INNER JOIN Van_Minivan ON Vehicle.vin = Van_Minivan.vin 
                    UNION
                    SELECT Vehicle.*, "SUV" AS type FROM Vehicle INNER JOIN SUV ON Vehicle.vin = SUV.vin) AS Temp0 ON Temp0.vin = Vehicle.vin) AS sold_vehicle) AS sd_all
        WHERE sd_all.sell_days >0
        GROUP BY sd_all.type) AS all_sold_data ON AVT.type = all_sold_data.type

        LEFT JOIN

        (SELECT sd365.type, COUNT(sd365.vin) AS sold_in_year FROM (
            SELECT sold_vehicle.vin, sold_vehicle.type, DATEDIFF('%s', sold_vehicle.purchase_date) AS sell_days 
            FROM (
                SELECT Temp0.type, Sale.vin, Sale.purchase_date
        FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin 
        LEFT JOIN (SELECT Vehicle.*, "Car" AS type FROM Vehicle INNER JOIN Car ON Vehicle.vin = Car.vin
                    UNION
                    SELECT Vehicle.*, "Convertible" AS type FROM Vehicle INNER JOIN Convertible ON Vehicle.vin = Convertible.vin 
                    UNION
                    SELECT Vehicle.*, "Truck" AS type FROM Vehicle INNER JOIN Truck ON Vehicle.vin = Truck.vin
                    UNION
                    SELECT Vehicle.*, "Van" AS type FROM Vehicle INNER JOIN Van_Minivan ON Vehicle.vin = Van_Minivan.vin 
                    UNION
                    SELECT Vehicle.*, "SUV" AS type FROM Vehicle INNER JOIN SUV ON Vehicle.vin = SUV.vin) AS Temp0 ON Temp0.vin = Vehicle.vin) AS sold_vehicle) AS sd365
        WHERE sd365.sell_days <=365
        GROUP BY sd365.type) AS year_sold_data ON all_sold_data.type = year_sold_data.type

        LEFT JOIN

        (SELECT sd30.type, COUNT(sd30.vin) AS sold_in_month FROM (
            SELECT sold_vehicle.vin, sold_vehicle.type, DATEDIFF('%s', sold_vehicle.purchase_date) AS sell_days 
            FROM (
                SELECT Temp0.type, Sale.vin, Sale.purchase_date
        FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin 
        LEFT JOIN (SELECT Vehicle.*, "Car" AS type FROM Vehicle INNER JOIN Car ON Vehicle.vin = Car.vin
                    UNION
                    SELECT Vehicle.*, "Convertible" AS type FROM Vehicle INNER JOIN Convertible ON Vehicle.vin = Convertible.vin 
                    UNION
                    SELECT Vehicle.*, "Truck" AS type FROM Vehicle INNER JOIN Truck ON Vehicle.vin = Truck.vin
                    UNION
                    SELECT Vehicle.*, "Van" AS type FROM Vehicle INNER JOIN Van_Minivan ON Vehicle.vin = Van_Minivan.vin 
                    UNION
                    SELECT Vehicle.*, "SUV" AS type FROM Vehicle INNER JOIN SUV ON Vehicle.vin = SUV.vin) AS Temp0 ON Temp0.vin = Vehicle.vin) AS sold_vehicle) AS sd30
        WHERE sd30.sell_days <=30
        GROUP BY sd30.type) AS month_sold_data ON all_sold_data.type = month_sold_data.type);""" %(yymmdd, yymmdd, yymmdd))


    cur.execute(query_sale_by_type)
    sale_by_type = cur.fetchall()
    db_close(connection, cur)

    # print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',sale_by_type, type(sale_by_type))
    sale_by_type_out = []
    for row in sale_by_type:
        # print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', row, type(row))
        # print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', row[0], type(row[0]))
        if row[0] is None:
            continue
        else:
            sale_by_type_out.append(row)

    return render_template('sales_by_type_report.html', data=sale_by_type_out, logged_in=login_user_name, user_role=user_role)


@app.route('/sales_by_maker_report', methods=['GET', 'POST'])
def sales_by_maker():
    connection = df_connect(config)
    cur = db_cursor(connection)
    sale_by_maker = ""
    latest_sale_date = ""

    query_latest_sale_date = """
    SELECT DATE_FORMAT(MAX(purchase_date), '%Y-%m-%d') FROM Sale;"""

    cur.execute(query_latest_sale_date)

    latest_sale_date = cur.fetchall()
    # print(latest_sale_date, type(latest_sale_date))

    yymmdd = latest_sale_date[0][0]
    # print(yymmdd, type(yymmdd))

    query_sale_by_maker = ('''
    SELECT all_sold_data.manufacturer_name, IFNULL(month_sold_data.sold_in_month,0), IFNULL(year_sold_data.sold_in_year,0), IFNULL(all_sold_data.sold_all_time,0) FROM(
        (SELECT sd_all.manufacturer_name, COUNT(sd_all.vin) AS sold_all_time FROM (
            SELECT sold_vehicle.vin, sold_vehicle.manufacturer_name, DATEDIFF('%s', sold_vehicle.purchase_date) AS sell_days 
            FROM (
                SELECT Vehicle.manufacturer_name, Sale.vin, Sale.purchase_date 
        FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin) AS sold_vehicle) AS sd_all
        WHERE sd_all.sell_days >0
        GROUP BY sd_all.manufacturer_name) AS all_sold_data

        LEFT JOIN

        (SELECT sd365.manufacturer_name, COUNT(sd365.vin) AS sold_in_year FROM (
            SELECT sold_vehicle.vin, sold_vehicle.manufacturer_name, DATEDIFF('%s', sold_vehicle.purchase_date) AS sell_days 
            FROM (
                SELECT Vehicle.manufacturer_name, Sale.vin, Sale.purchase_date 
        FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin) AS sold_vehicle) AS sd365
        WHERE sd365.sell_days <=365
        GROUP BY sd365.manufacturer_name) AS year_sold_data ON all_sold_data.manufacturer_name = year_sold_data.manufacturer_name

        LEFT JOIN

        (SELECT sd30.manufacturer_name, COUNT(sd30.vin) AS sold_in_month FROM (
            SELECT sold_vehicle.vin, sold_vehicle.manufacturer_name, DATEDIFF('%s', sold_vehicle.purchase_date) AS sell_days 
            FROM (
                SELECT Vehicle.manufacturer_name, Sale.vin, Sale.purchase_date 
        FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin) AS sold_vehicle) AS sd30
        WHERE sd30.sell_days <=30
        GROUP BY sd30.manufacturer_name) AS month_sold_data ON all_sold_data.manufacturer_name = month_sold_data.manufacturer_name);''' %(yymmdd,yymmdd,yymmdd))

    cur.execute(query_sale_by_maker)
    sale_by_maker = cur.fetchall()
    db_close(connection, cur)
    return render_template('sales_by_maker_report.html', data=sale_by_maker, logged_in=login_user_name, user_role=user_role)


@app.route('/income_report', methods=['GET', 'POST'])
def income_report():
    gross_income = ''
    connection = df_connect(config)
    cur = db_cursor(connection)

    query_gross_income = '''
        SELECT C_list.customer_ID, C_list.c_name, LEAST(Sale_Record.min_buy, Repair_Record.min_buy), GREATEST(Sale_Record.max_buy, Repair_Record.max_buy), Sale_Record.n_sale, Repair_Record.n_repair, (Sale_Record.sale_total_income + Repair_Record.repair_total_income) AS total_income
        FROM (
        SELECT CONCAT(Customer_Person.first_name, " ", Customer_Person.last_name) AS c_name, Customer.customer_ID
        FROM Customer INNER JOIN Customer_Person ON Customer.customer_ID = Customer_Person.customer_ID
        UNION
        SELECT Customer_Business.business_name AS c_name, Customer.customer_ID
        FROM Customer INNER JOIN Customer_Business ON Customer.customer_ID = Customer_Business.customer_ID) AS C_list
        LEFT JOIN
        (SELECT S_customer.customer_ID, S_customer.min_buy, S_customer.max_buy, S_customer.n_sale, S_customer.sale_total_income FROM
        (SELECT Sale.customer_ID, MIN(Sale.purchase_date) AS min_buy, MAX(Sale.purchase_date) AS max_buy, COUNT(Sale.vin) AS n_sale, ROUND(SUM(Sale.sold_price),2) AS sale_total_income
        FROM Sale 
        GROUP BY Sale.customer_ID) AS S_customer) AS Sale_Record ON C_list.customer_ID = Sale_Record.customer_ID
        LEFT JOIN 
        (SELECT R_customer.customer_ID, R_customer.min_buy, R_customer.max_buy, R_customer.n_repair, R_customer.repair_total_income FROM
        (SELECT Repair.customer_ID, MIN(Repair.start_date) AS min_buy, MAX(Repair.completed_date) AS max_buy, COUNT(Repair.repair_ID) AS n_repair, ROUND(SUM(Repair.labor_charge)+SUM(UP.total_part_cost), 2) AS repair_total_income
        FROM Repair INNER JOIN (SELECT SUM(Used_Part.part_cost) AS total_part_cost, Used_Part.repair_ID
                    FROM Used_Part GROUP BY repair_ID) AS UP ON Repair.repair_ID = UP.repair_ID 
        GROUP BY Repair.customer_ID) AS R_customer) AS Repair_Record ON C_list.customer_ID = Repair_Record.customer_ID
        ORDER BY total_income DESC
        LIMIT 15;'''
    cur.execute(query_gross_income)
    gross_income = cur.fetchall()
    db_close(connection, cur)
    return render_template('income_report.html', data=gross_income, logged_in=login_user_name, user_role=user_role)


@app.route('/customer_repair_datail/<value>', methods=['GET', 'POST'])
def customer_repair_detail(value):
    repair_income = ""
    customer_ID = ""
    item_list = ''
    row_list = value.split(',')
    print('$$$$$$$$$$$$$$$$$', row_list)
    print('aaaaaaaaaaa', row_list[0])
    item_list = row_list[0]
    customer_ID = int(item_list[1:])
    print('```````````````````', type(customer_ID), customer_ID)

    connection = df_connect(config)
    cur = db_cursor(connection)

    query_repair_income = ("""
    SELECT C_list.c_name, Repair.start_date, Repair.completed_date, Repair.vin, Repair.odometer, ROUND(UP.total_part_cost,2), ROUND(Repair.labor_charge,2), ROUND((UP.total_part_cost + Repair.labor_charge),2) AS total_cost, CONCAT(Login_User.first_name, " ", Login_User.last_name) AS s_name

    FROM Repair INNER JOIN (SELECT SUM(Used_Part.part_cost) AS total_part_cost, Used_Part.repair_ID
                FROM Used_Part GROUP BY repair_ID) AS UP ON Repair.repair_ID = UP.repair_ID 
    LEFT JOIN 
    (SELECT CONCAT(Customer_Person.first_name, " ", Customer_Person.last_name) AS c_name, Customer.customer_ID
    FROM Customer INNER JOIN Customer_Person ON Customer.customer_ID = Customer_Person.customer_ID
    UNION
    SELECT Customer_Business.business_name AS c_name, Customer.customer_ID
    FROM Customer INNER JOIN Customer_Business ON Customer.customer_ID = Customer_Business.customer_ID) AS C_list ON Repair.customer_ID = C_list.customer_ID
    LEFT JOIN Login_User ON Repair.userID = Login_User.userID
    WHERE Repair.customer_ID = '%d'
    ORDER BY Repair.start_date DESC, Repair.completed_date DESC, Repair.vin ASC;""" % (customer_ID))
    cur.execute(query_repair_income)
    repair_income = cur.fetchall()
    db_close(connection, cur)
    return render_template('customer_repair_detail.html', data=repair_income, logged_in=login_user_name, user_role=user_role)


@app.route('/customer_sales_datail/<value>', methods=['GET', 'POST'])
def customer_sales_detail(value):
    sales_income = ""
    customer_ID = ""
    item_list = ''
    row_list = value.split(',')
    print('$$$$$$$$$$$$$$$$$', row_list)
    print('aaaaaaaaaaa', row_list[0])
    item_list = row_list[0]
    customer_ID = int(item_list[1:])
    print('```````````````````', type(customer_ID), customer_ID)

    connection = df_connect(config)
    cur = db_cursor(connection)
    query_sales_income = ("""
    SELECT C_list.c_name, Sale.purchase_date, ROUND(Sale.sold_price,2), Sale.vin, Vehicle.model_year, Vehicle.model_name, Vehicle.manufacturer_name, CONCAT(Login_User.first_name, " ", Login_User.last_name) AS s_name
    FROM Sale LEFT JOIN Vehicle ON Sale.vin = Vehicle.vin
    LEFT JOIN 
    (SELECT CONCAT(Customer_Person.first_name, " ", Customer_Person.last_name) AS c_name, Customer.customer_ID
    FROM Customer INNER JOIN Customer_Person ON Customer.customer_ID = Customer_Person.customer_ID
    UNION
    SELECT Customer_Business.business_name AS c_name, Customer.customer_ID
    FROM Customer INNER JOIN Customer_Business ON Customer.customer_ID = Customer_Business.customer_ID) AS C_list ON Sale.customer_ID = C_list.customer_ID
    LEFT JOIN Login_User ON Sale.userID = Login_User.userID
    WHERE Sale.customer_ID = (%d)
    ORDER BY Sale.purchase_date DESC, Sale.vin ASC;""" % (customer_ID))
    cur.execute(query_sales_income)
    sales_income = cur.fetchall()
    db_close(connection, cur)

    return render_template('customer_sales_detail.html', data=sales_income, logged_in=login_user_name, user_role=user_role)


@app.route('/repairs_report', methods=['GET', 'POST'])
def repairs_report():
    repair_by_manufacturer = ""
    connection = df_connect(config)
    cur = db_cursor(connection)
    query_repair_by_manufacturer = """
    SELECT Vehicle.manufacturer_name, COUNT(Repair.repair_ID), ROUND(SUM(UP.total_part_cost),2), ROUND(SUM(Repair.labor_charge),2), ROUND((SUM(UP.total_part_cost) + SUM(Repair.labor_charge)),2) AS total_charge
    FROM Vehicle INNER JOIN Repair ON Vehicle.vin = Repair.vin
    INNER JOIN (SELECT SUM(Used_Part.part_cost) AS total_part_cost, Used_Part.repair_ID
                FROM Used_Part GROUP BY repair_ID) AS UP ON Repair.repair_ID = UP.repair_ID
    GROUP BY Vehicle.manufacturer_name 
    ORDER BY Vehicle.manufacturer_name ASC;"""
    cur.execute(query_repair_by_manufacturer)
    repair_by_manufacturer = cur.fetchall()
    db_close(connection, cur)
    return render_template('repairs_report.html', data=repair_by_manufacturer, logged_in=login_user_name, user_role=user_role)


@app.route('/repair_by_type/<value>', methods=['GET', 'POST'])
def repair_by_type(value):
    repair_by_type = ""
    # print('*********************************', value, type(value))

    row_list = value.split(',')
    # print('$$$$$$$$$$$$$$$$$',row_list)
    # print('aaaaaaaaaaa', row_list[0])
    item_list = row_list[0].split("'")
    # print(item_list)
    vehicle_manufacturer = item_list[1]
    # print('```````````````````', type(vehicle_manufacturer), vehicle_manufacturer)

    connection = df_connect(config)
    cur = db_cursor(connection)
    query_repair_by_type = ("""
    SELECT Vehicle.manufacturer_name, Temp0.type, COUNT(Repair.repair_ID) AS r_number, ROUND(SUM(UP.total_part_cost),2), ROUND(SUM(Repair.labor_charge),2), ROUND((SUM(UP.total_part_cost) + SUM(Repair.labor_charge)),2) AS total_charge
    FROM Vehicle INNER JOIN Repair ON Vehicle.vin = Repair.vin
    INNER JOIN (SELECT SUM(Used_Part.part_cost) AS total_part_cost, Used_Part.repair_ID
                FROM Used_Part GROUP BY repair_ID) AS UP ON Repair.repair_ID = UP.repair_ID
    LEFT JOIN (SELECT Vehicle.*, "Car" AS type FROM Vehicle INNER JOIN Car ON Vehicle.vin = Car.vin
        UNION
        SELECT Vehicle.*, "Convertible" AS type FROM Vehicle INNER JOIN Convertible ON Vehicle.vin = Convertible.vin UNION
        SELECT Vehicle.*, "Truck" AS type FROM Vehicle
        INNER JOIN Truck ON Vehicle.vin = Truck.vin
        UNION
        SELECT Vehicle.*, "Van" AS type FROM Vehicle
        INNER JOIN Van_Minivan ON Vehicle.vin = Van_Minivan.vin 
        UNION
        SELECT Vehicle.*, "SUV" AS type FROM Vehicle
        INNER JOIN SUV ON Vehicle.vin = SUV.vin) AS Temp0 ON Temp0.vin = Vehicle.vin
    WHERE Vehicle.manufacturer_name = '%s'
    GROUP BY Temp0.type
    ORDER BY Temp0.type ASC, r_number DESC;""" % (vehicle_manufacturer))
    cur.execute(query_repair_by_type)
    repair_by_type = cur.fetchall()
    db_close(connection, cur)

    return render_template('repair_by_type.html', data=repair_by_type, logged_in=login_user_name, user_role=user_role)


@app.route('/repair_by_model/<value>', methods=['GET', 'POST'])
def repair_by_model(value):
    repair_by_model = ""
    # print('*********************************', value, type(value))
    vehicle_manufacturer = ''
    vehicle_type = ''

    row_list = value.split(',')
    # print('$$$$$$$$$$$$$$$$$',row_list)
    # print('aaaaaaaaaaa', row_list[0])
    type_list = row_list[1].split("'")
    # print(type_list)
    vehicle_type = type_list[1]
    maker_list = row_list[0].split("'")
    vehicle_manufacturer = maker_list[1]
    # print('```````````````````', vehicle_type, vehicle_manufacturer)
    connection = df_connect(config)
    cur = db_cursor(connection)
    query_repair_by_model = ("""
    SELECT Vehicle.manufacturer_name, Temp0.type, Vehicle.model_name, COUNT(Repair.repair_ID) AS r_number, ROUND(SUM(UP.total_part_cost),2), ROUND(SUM(Repair.labor_charge),2), ROUND((SUM(UP.total_part_cost) + SUM(Repair.labor_charge)),2) AS total_charge 
    FROM Vehicle INNER JOIN Repair ON Vehicle.vin = Repair.vin
    INNER JOIN (SELECT SUM(Used_Part.part_cost) AS total_part_cost, Used_Part.repair_ID
                FROM Used_Part GROUP BY repair_ID) AS UP ON Repair.repair_ID = UP.repair_ID
    LEFT JOIN (SELECT Vehicle.*, "Car" AS type FROM Vehicle INNER JOIN Car ON Vehicle.vin = Car.vin
            UNION
            SELECT Vehicle.*, "Convertible" AS type FROM Vehicle INNER JOIN Convertible ON Vehicle.vin = Convertible.vin UNION
            SELECT Vehicle.*, "Truck" AS type FROM Vehicle
            INNER JOIN Truck ON Vehicle.vin = Truck.vin
            UNION
            SELECT Vehicle.*, "Van" AS type FROM Vehicle
            INNER JOIN Van_Minivan ON Vehicle.vin = Van_Minivan.vin UNION
            SELECT Vehicle.*, "SUV" AS type FROM Vehicle
            INNER JOIN SUV ON Vehicle.vin = SUV.vin) AS Temp0 ON Temp0.vin = Vehicle.vin
    WHERE Vehicle.manufacturer_name = '%s' and Temp0.type = '%s'
    GROUP BY Vehicle.model_name
    ORDER BY Vehicle.model_name ASC, r_number DESC;""" % (vehicle_manufacturer, vehicle_type))
    cur.execute(query_repair_by_model)
    repair_by_model = cur.fetchall()
    db_close(connection, cur)
    return render_template('repair_by_model.html', data=repair_by_model, logged_in=login_user_name, user_role=user_role)


@app.route('/below_cost_sales_report', methods=['GET', 'POST'])
def below_cost_sales_report():
    connection = df_connect(config)
    cur = db_cursor(connection)
    below_cost_sales = ""
    no_matching_error = "No sales record"
    query_below_cost_sales = '''
    SELECT Sale.purchase_date, ROUND(Vehicle.invoice_price,2), ROUND(Sale.sold_price,2), ROUND(Sale.sold_Price / Vehicle.invoice_price, 2) AS ratio, C_list.c_name, CONCAT(Login_User.first_name, " ", Login_User.last_name) AS s_name
    FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin
    LEFT JOIN (
        SELECT CONCAT(first_name, " ", last_name) AS c_name, Customer.customer_ID
        FROM Customer INNER JOIN Customer_Person ON Customer.customer_ID = Customer_Person.customer_ID
        UNION
        SELECT business_name AS c_name, Customer.customer_ID
        FROM Customer INNER JOIN Customer_Business ON Customer.customer_ID = Customer_Business.customer_ID) AS C_list ON Sale.customer_ID = C_list.customer_ID
    LEFT JOIN Login_User ON Sale.userID = Login_User.userID
    ORDER BY Sale.purchase_date DESC, ratio DESC;'''
    cur.execute(query_below_cost_sales)
    below_cost_sales = cur.fetchall()
    print("888888888888888888888", below_cost_sales, type(below_cost_sales))

    if len(below_cost_sales) > 0:
        no_matching_error = ""

    else:
        no_matching_error = "No sales record"

    db_close(connection, cur)
    return render_template('below_cost_sales_report.html', data=below_cost_sales, error_messages=(no_matching_error), logged_in=login_user_name, user_role=user_role)


@app.route('/ave_time_inventory_report', methods=['GET', 'POST'])
def ave_time_inventory_report():
    connection = df_connect(config)
    cur = db_cursor(connection)
    ave_time_inventory = ""
    no_matching_error = "no vehicle record"
    query_ave_time_inventory = """
        SELECT IFNULL(ROUND(AVG(DATEDIFF(Sale.purchase_date, Temp0.added_dttm)), 0), "N/A") AS inventory_time, Temp0.type 
        FROM (SELECT Vehicle.*, "Car" AS type FROM Vehicle INNER JOIN Car ON Vehicle.vin = Car.vin
            UNION
            SELECT Vehicle.*, "Convertible" AS type FROM Vehicle INNER JOIN Convertible ON Vehicle.vin = Convertible.vin UNION
            SELECT Vehicle.*, "Truck" AS type FROM Vehicle
            INNER JOIN Truck ON Vehicle.vin = Truck.vin
            UNION
            SELECT Vehicle.*, "Van" AS type FROM Vehicle
            INNER JOIN Van_Minivan ON Vehicle.vin = Van_Minivan.vin UNION
            SELECT Vehicle.*, "SUV" AS type FROM Vehicle
            INNER JOIN SUV ON Vehicle.vin = SUV.vin) AS Temp0 LEFT JOIN Sale ON Temp0.vin = Sale.vin
        GROUP BY Temp0.type;"""
    cur.execute(query_ave_time_inventory)
    ave_time_inventory = cur.fetchall()

    ave_time_inventory_out = []
    for d in ave_time_inventory:
        if d[0] is None:
            d[0] = "N/A"  # show N/A if no vehicle sold in that type
        else:
            no_matching_error = 'no record'
        ave_time_inventory_out.append(d)

    if len(ave_time_inventory_out) > 0:
        no_matching_error = ''
    db_close(connection, cur)
    return render_template('ave_time_inventory_report.html', data=ave_time_inventory_out, error_messages=(no_matching_error), logged_in=login_user_name, user_role=user_role)


@app.route('/parts_report', methods=['GET', 'POST'])
def parts_report():
    connection = df_connect(config)
    cur = db_cursor(connection)
    query_part_statistic = """
    SELECT Used_Part.part_vendor, ROUND(SUM(Used_Part.part_quantity),0) AS supplied_number, ROUND(SUM(Used_Part.part_cost),2) AS spent_amount
    FROM Used_Part
    GROUP BY Used_Part.part_vendor
    ORDER BY supplied_number DESC;"""
    cur.execute(query_part_statistic)
    part_statistic = cur.fetchall()
    db_close(connection, cur)
    return render_template('parts_report.html', data=part_statistic, logged_in=login_user_name, user_role=user_role)


@app.route('/monthly_sales_report', methods=['GET', 'POST'])
def monthly_sales_report():
    monthly_sales_report = ""
    no_matching_error = ''
    connection = df_connect(config)
    cur = db_cursor(connection)
    # query_monthly_sales = """
    # SELECT COUNT(Sale.vin), ROUND(SUM(Sale.sold_price),2) AS total_sales_income, ROUND((SUM(Sale.sold_price)-SUM(Vehicle.invoice_price)),2) AS total_net_income, ROUND(AVG(((Sale.sold_price)/(Vehicle.invoice_price))), 1)*100 AS ratio, DATE_FORMAT(Sale.purchase_date, '%Y-%m')
    # FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin
    # GROUP BY DATE_FORMAT(Sale.purchase_date, '%Y-%m')
    # ORDER BY DATE_FORMAT(Sale.purchase_date, '%Y-%m') DESC;"""
    query_monthly_sales = """
    SELECT COUNT(Sale.vin), SUM(Sale.sold_price) AS total_sales_income, (SUM(Sale.sold_price)-SUM(Vehicle.invoice_price)) AS total_net_income, ROUND((SUM(Sale.sold_price))/(SUM(Vehicle.invoice_price)), 1)*100 AS ratio, DATE_FORMAT(Sale.purchase_date, '%Y-%m')
    FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin
    GROUP BY DATE_FORMAT(Sale.purchase_date, '%Y-%m')
    ORDER BY DATE_FORMAT(Sale.purchase_date, '%Y-%m') DESC;"""
    cur.execute(query_monthly_sales)
    monthly_sales_report = cur.fetchall()
    db_close(connection, cur)

    if monthly_sales_report == "":
        no_matching_error = 'No sales record'
    return render_template('monthly_sales_report.html', data=monthly_sales_report, error_messages=(no_matching_error), logged_in=login_user_name, user_role=user_role)


@app.route('/monthly_salesperson_kpi/<value>', methods=['GET', 'POST'])
def monthly_salesperson_kpi(value):
    monthly_salesperson_kpi = ""
    mmyy = ""

    # print('*********************************', value, type(value))

    row_list = value.split(',')
    # print('$$$$$$$$$$$$$$$$$',row_list)
    # print('aaaaaaaaaaa', row_list[4])
    item_list = row_list[4].split("'")
    # print(item_list)
    mmyy = str(item_list[1])
    # print('```````````````````', type(mmyy), mmyy)

    connection = df_connect(config)
    cur = db_cursor(connection)

    query_monthly_salesperson_kpi = ('''
    SELECT Login_User.first_name, Login_User.last_name, COUNT(Sale.vin) AS sold_number, ROUND(SUM(Sale.sold_price),2) AS total_sales_income
    FROM Sale INNER JOIN Vehicle ON Sale.vin = Vehicle.vin
    LEFT JOIN Login_User ON Sale.userID = Login_User.userID
    WHERE DATE_FORMAT(Sale.purchase_date, '%%Y-%%m') = '%s'
    GROUP BY Sale.userID
    ORDER BY sold_number DESC, total_sales_income DESC;''' % (mmyy))

    # print(query_monthly_salesperson_kpi)

    cur.execute(query_monthly_salesperson_kpi)
    monthly_salesperson_kpi = cur.fetchall()
    # print(monthly_salesperson_kpi)
    db_close(connection, cur)
    return render_template('monthly_salesperson_kpi.html', data=monthly_salesperson_kpi, logged_in=login_user_name, user_role=user_role)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
